//
//  HostedTableViewController.swift
//  Picme
//
//  Created by John Nguyen on 5/24/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class HostedTableViewController: PFQueryTableViewController {

    
    var eventId = ""
    var hostId = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Configure the PFQueryTableView
        self.parseClassName = "Events"
        
        self.pullToRefreshEnabled = true
        self.paginationEnabled = true
        self.objectsPerPage = 3
    }
    
    
    override func viewWillAppear(animated: Bool) {
        loadObjects()
    }
    
    
    override func objectsDidLoad(error: NSError!) {
        super.objectsDidLoad(error)
    }
    
    override func objectAtIndexPath(indexPath: NSIndexPath!) -> PFObject? {
        if(indexPath.section < self.objects!.count){
            return self.objects![indexPath.section] as? PFObject
        }else{
            return nil
        }
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section == self.objects!.count){
            return nil
        }
        let sectionHeaderView : HeaderTableViewCell = tableView.dequeueReusableCellWithIdentifier("SectionHeaderCell") as! HeaderTableViewCell
        
        let profileImageView : PFImageView = sectionHeaderView.viewWithTag(1) as! PFImageView
        let usernameLabel : UILabel = sectionHeaderView.viewWithTag(2) as! UILabel
        let descriptionLabel : KILabel = sectionHeaderView.viewWithTag(3) as! KILabel
        let voteLabel : UILabel = sectionHeaderView.viewWithTag(4) as! UILabel
     
        descriptionLabel.systemURLStyle = true
        descriptionLabel.hashtagLinkTapHandler = { label, hashtag, range in
            let alert : UIAlertController = UIAlertController(title: "Tapped" + hashtag, message: "Tapped" + hashtag, preferredStyle: UIAlertControllerStyle.Alert)
            NSLog("Hashtah \(hashtag) tapped")
        }
        
        let event : PFObject = self.objects![section] as! PFObject
        let user : PFUser = event["hostUser"] as! PFUser
        let title : NSString = event["eventTitle"] as! NSString
        let profilePicture:PFFile = user["profileImage"] as! PFFile
        let vote : NSInteger = event["Likes"] as! NSInteger
        let description : NSString = event["eventDescription"] as! NSString
        let eventID : String = event.objectId!
        let hostID : String = user.objectId!
        eventId = eventID
        usernameLabel.text = title as String
        descriptionLabel.text = description as String
        profileImageView.file = profilePicture
        voteLabel.text = String(vote)
        profileImageView.layer.cornerRadius = 20
        profileImageView.layer.masksToBounds = true
        profileImageView.loadInBackground(nil)
        
        return sectionHeaderView
    }
    
    override func tableView(tableView: (UITableView!), cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject!) -> PFTableViewCell? {
        if(indexPath.section == self.objects!.count){
            var cell:LoadTableViewCell = self.tableView(tableView, cellForNextPageAtIndexPath: indexPath)! as! LoadTableViewCell
            return cell
        }else{
            let imageCell = tableView.dequeueReusableCellWithIdentifier("PhotoCell") as! BodyTableViewCell
            
            //            let eventImageView : PFImageView = imageCell.viewWithTag(1) as! PFImageView
            var tapEvent = UITapGestureRecognizer(target: self, action: "tappedEvent:")
            eventId = object.objectId!
            imageCell.eventImageView.file = object.objectForKey("eventPicture") as? PFFile
            //Life is not pretending to be busy , it's beeing busy if you need to and not beeing busy when you need to reflect on life.
            imageCell.eventImageView.loadInBackground(nil)
            
            
            return imageCell
        }
    }
    
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var sections:NSInteger = self.objects!.count
        if(self.paginationEnabled && sections > 0){
            sections++
        }
        return sections
        
    }
    
    override func _indexPathForPaginationCell() -> NSIndexPath {
        return NSIndexPath(forRow: 0, inSection: self.objects!.count)
    }
    
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == self.objects!.count){
            return 0
        }
        return 50.0
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == self.objects!.count){
            return 0
        }
        return 50.0
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == self.objects!.count){
            return 50.0
        }
        return 320.0
    }
    
    
    
    override func tableView(tableView: UITableView, cellForNextPageAtIndexPath indexPath: (NSIndexPath!)) -> PFTableViewCell? {
        var loadMoreCell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell") as! PFTableViewCell
        return loadMoreCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == self.objects!.count && self.paginationEnabled){
            self.loadNextPage()
        }else{
            println("Pressed a cell")
        }
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = tableView.dequeueReusableCellWithIdentifier("FooterView") as! UITableViewCell
        
        return footerView
    }
    
    override func queryForTable() -> PFQuery {
        let query: PFQuery = PFQuery(className: self.parseClassName!)
        query.includeKey("hostUser")
        query.whereKey("hostUser", equalTo: PFUser.objectWithoutDataWithObjectId(hostId))
        query.orderByDescending("createdAt")
        return query
    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
