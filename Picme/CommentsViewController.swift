//
//  CommentsViewController.swift
//  Picme
//
//  Created by John Nguyen on 6/18/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class CommentsViewController: UIViewController {
    
    var Id = ""

    @IBOutlet weak var commentTextField: UITextField!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func close(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func send(sender: AnyObject) {
         var comment : PFObject = PFObject(className: "Comments")
        comment["text"] = commentTextField.text
        comment["commentId"] = Id
        comment["author"] = PFUser.currentUser()
        comment["likes"] = 0
        comment.saveInBackgroundWithBlock { (success, error) -> Void in
            if(success){
                NSNotificationCenter.defaultCenter().postNotificationName("load", object: nil)
                print("success")
                self.commentTextField.text = " "
            }
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "sendToComments"){
            let destinationViewController = segue.destinationViewController as! CommentsTableViewController
            destinationViewController.Id = Id
        }
    }
    

    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
