//
//  ProfileViewController.swift
//  Picme
//
//  Created by John Nguyen on 5/23/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit
import Parse
import ParseUI
class ProfileViewController: UIViewController , UITableViewDelegate  {
    
    
    
    @IBOutlet weak var profileImageView: PFImageView!
    @IBOutlet weak var usernameTextField: UILabel!
    @IBOutlet weak var descriptionTextField: UILabel!
    @IBOutlet weak var backgroundImageView: PFImageView!
    var profileId = PFUser.currentUser()?.objectId
    @IBOutlet weak var subscribeButton: UIButton!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var settingsButton: UIButton!
    let count = 0
    var eventId = ""
    var subscriptionArray = NSMutableArray()
    
    override func viewDidLoad() {
        super.viewDidLoad()
       
        
        if(profileId == PFUser.currentUser()?.objectId){
            usernameTextField.text = PFUser.currentUser()?.username
            descriptionTextField.text = PFUser.currentUser()?.valueForKey("profileDescription") as? String
            profileImageView.file = PFUser.currentUser()?.valueForKey("profileImage") as? PFFile
            backgroundImageView.file = PFUser.currentUser()?.valueForKey("profileBackground") as? PFFile
            profileImageView.layer.cornerRadius = 25
            profileImageView.layer.masksToBounds = true
            profileImageView.loadInBackground(nil)
            backgroundImageView.loadInBackground(nil)
            subscribeButton.hidden = true
        }else{
            let query : PFQuery = PFQuery(className: "Subscriptions")
            query.whereKey("fromUser", equalTo: PFUser.currentUser()!)
            query.findObjectsInBackgroundWithBlock({ (activities, error) -> Void in
                if((error) == nil){
                    if(activities?.count > 0){
                        for activity in activities!{
                            let subscription :PFObject = activity["toUser"] as! PFObject
                            //self.likeArray.addObject(sub.objectId!)
                            println("Profile id: " + self.profileId!)
                            println("fromUser id : " + subscription.objectId!)
                            if( subscription.objectId == self.profileId){
                                self.subscribeButton.selected = true
                            }
                            //Learn ways of how to implement this
                        }
                    }
                
                }else{
                    println(error)
                }
            })
        let object : PFObject = PFUser.objectWithoutDataWithObjectId(profileId)

            usernameTextField.text = object["username"] as? String
            descriptionTextField.text = object["profileDescription"] as? String
            profileImageView.file = object["profileImage"] as? PFFile
            backgroundImageView.file = object["profileBackground"] as? PFFile
            profileImageView.layer.cornerRadius = 25
            profileImageView.layer.masksToBounds = true
            profileImageView.loadInBackground(nil)
            backgroundImageView.loadInBackground(nil)
            settingsButton.hidden = true
        }
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    }
    
    func subscribe(user : PFObject){
    
    }
    
    
    func unsubscribe(user : PFObject){
    
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "profileToHosted"){
            
            let destinationViewController = segue.destinationViewController as! HostedTableViewController
            
            destinationViewController.hostId = profileId!
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func clickSubscribe(sender: AnyObject) {
        subscribeButton.selected = !subscribeButton.selected.boolValue
        
        if(subscribeButton.selected == true){
            //call the subscribe function
            let activity : PFObject = PFObject(className: "Subscriptions")
            activity["toUser"] = PFUser.objectWithoutDataWithObjectId(profileId)
            activity["fromUser"] = PFUser.currentUser()
            activity.save()
            
        }else{
            //call the unsubscribe function
            let query : PFQuery = PFQuery(className: "Subscriptions")
            query.whereKey("toUser", equalTo: PFUser.objectWithoutDataWithObjectId(profileId))
            query.whereKey("fromUser", equalTo: PFUser.currentUser()!)
            
            query.findObjectsInBackgroundWithBlock({ (subscriptions, error) -> Void in
                if(error == nil){
                    for subscription in subscriptions!{
                        subscription.delete()
                    }
                }
            })
        }
    }
    

}
