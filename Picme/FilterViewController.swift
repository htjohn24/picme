//
//  FilterViewController.swift
//  Picme
//
//  Created by John Nguyen on 9/6/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit


protocol FilterViewDelegate {
    func updateData(data: NSDictionary)
}

class FilterViewController: UIViewController {

    var promixity = 50
    var groupSize = 50
    var date = 1
    var delegate:FilterViewDelegate? = nil
    var eduBool = false
    var verifiedBool = false

    @IBOutlet weak var locationValue: UILabel!
    @IBOutlet weak var groupValue: UILabel!
    @IBOutlet weak var locationSlider: UISlider!
    @IBOutlet weak var groupSlider: UISlider!
    
    @IBOutlet weak var eduSwitch: UISwitch!
    
    @IBOutlet weak var veriSwitch: UISwitch!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        locationSlider.value = Float(promixity)
        locationValue.text = String(stringInterpolationSegment: promixity)
        
        groupSlider.value = Float(groupSize)
        groupValue.text = String(groupSize)
        
        if(eduBool == true){
            eduSwitch.setOn(true, animated: true)
        }else{
            eduSwitch.setOn(false, animated: true)
        }
        
        if(verifiedBool == true){
            veriSwitch.setOn(true, animated: true)
        }else{
            veriSwitch.setOn(false, animated: true)
        }
        self.navigationController?.navigationBar.barTintColor = UIColor.whiteColor()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
    
    @IBAction func save(sender: AnyObject) {
        if (delegate != nil){
            let information: NSDictionary = ["proximity": self.promixity,"size" : self.groupSize,"date" : self.date , "edu" : self.eduBool , "verified" : self.verifiedBool]
            
            println("Proximity: " + String(self.promixity))
            delegate!.updateData(information)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }

    @IBAction func locationChanged(sender: UISlider) {
        promixity = Int(sender.value)
        locationValue.text = String(stringInterpolationSegment: self.promixity)
    }
    
    
    @IBAction func sizeChanged(sender: UISlider) {
        groupSize = Int(sender.value)
        groupValue.text = String(self.groupSize)
    }
    
    
    @IBAction func cancel(sender: AnyObject) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    
    }
    
    @IBAction func collegeSwitch(sender: AnyObject) {
        if (eduSwitch.on){
            eduBool = true
        }else{
            eduBool = false
        }
    }

    @IBAction func verifiedSwitch(sender: AnyObject) {
        if(veriSwitch.on){
            verifiedBool = true
        }else{
            verifiedBool = false
        }
    }
    
    
    @IBAction func `default`(sender: AnyObject) {
        locationValue.text = String(50)
        locationSlider.value = 50.0
        groupValue.text = String(50)
        groupSlider.value = 50.0
        eduSwitch.setOn(true, animated: true)
        veriSwitch.setOn(false, animated: true)
    }
    
    

}
