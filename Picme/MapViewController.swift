//
//  MapViewController.swift
//  Picme
//
//  Created by John Nguyen on 4/5/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

class MapViewController: UIViewController ,GMSMapViewDelegate , CLLocationManagerDelegate {
    var events = []
//    var mapView = GMSMapView()
    
    @IBOutlet weak var mapView: GMSMapView!
    @IBOutlet weak var partyButton: UIButton!
    @IBOutlet weak var sportsButton: UIButton!
    @IBOutlet weak var musicButton: UIButton!
    @IBOutlet weak var gameButton: UIButton!
    @IBOutlet weak var outdoorsButton: UIButton!
    @IBOutlet weak var otherButton: UIButton!
    
    var eventID = ""
    var eventDescription = "Picme Senpai"
    var eventVote = 0
    var eventAttendees = 0
    let locationManager =  CLLocationManager()
    @IBOutlet weak var menuButton: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        
        self.locationManager.requestAlwaysAuthorization()
        
        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }

        mapView.myLocationEnabled = true
        
        
        let query : PFQuery = PFQuery(className: "Events")
        query.includeKey("hostUser");
        query.findObjectsInBackgroundWithBlock {  (objects, error) -> Void in
            if error != nil {
                // There was an error
            } else {
                self.events = objects!
                self.setMarkerData(" ", all: true)
            }
        }
        
        // Do any additional setup after loading the view.
    }
    
    
    
    @IBAction func clear(sender: AnyObject) {
         setMarkerData("", all: true)
    }
    
    
    func locationManager(manager:CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus){
        if status == .AuthorizedWhenInUse{
            
            locationManager.startUpdatingLocation()
            mapView.myLocationEnabled = true
        }
    }
    
    func locationManager(manager:CLLocationManager!, didUpdateLocations locations:[AnyObject]!){
        if let location = locations.first as? CLLocation{
            
            mapView.camera = GMSCameraPosition(target:location.coordinate, zoom:15,bearing:0, viewingAngle:0)
            locationManager.stopUpdatingLocation()
        }
    }
    
    //Set Marker Data: goes into array of events and makes a marker for each event.
    func setMarkerData(filter: String , all : Bool){
        for event in events  {
             let eventType = event["eventType"] as! String
            if(all == true){
            mapView.delegate = self
            let xyCoord : PFGeoPoint = event["xyCoord"] as! PFGeoPoint
            let latitude : Double = xyCoord.latitude as Double
            let longitude : Double = xyCoord.longitude as Double
            let position = CLLocationCoordinate2D(latitude: latitude , longitude: longitude)
            var marker :GMSMarker = GMSMarker(position: position)
            let user : PFUser = event["hostUser"] as! PFUser
            marker.title = event["eventTitle"] as! String
            marker.snippet = event.objectId
            let imageData : PFFile = user["profileImage"] as! PFFile
            imageData.getDataInBackgroundWithBlock({   (imageData, error) -> Void in
                if !(error != nil) {
                    marker.userData = UIImage(data:imageData!)
                }
            })
            switch (event["eventType"]) as! String{
            case "food":
                marker.icon = UIImage(named:"food-icon")
            case "trail":
                marker.icon = UIImage(named: "trail-icon")
            case "sports":
                marker.icon = UIImage(named: "sports-icon")
            case "games":
                marker.icon = UIImage(named: "games-icon")
            case "edu":
                marker.icon = UIImage(named: "edu-icon")
            case "music":
                marker.icon = UIImage(named: "music-icon")
            default:
                marker.icon = UIImage(named: "otherMarker")
            }
                marker.map = mapView
            }else{
                if(eventType == filter){
                        mapView.delegate = self
                        let xyCoord : PFGeoPoint = event["xyCoord"] as! PFGeoPoint
                        let latitude : Double = xyCoord.latitude as Double
                        let longitude : Double = xyCoord.longitude as Double
                        let position = CLLocationCoordinate2D(latitude: latitude , longitude: longitude)
                        var marker :GMSMarker = GMSMarker(position: position)
                        let user : PFUser = event["hostUser"] as! PFUser
                        marker.title = event["eventTitle"] as! String
                        marker.snippet = event.objectId
                        let imageData : PFFile = user["profileImage"] as! PFFile
                        imageData.getDataInBackgroundWithBlock({   (imageData, error) -> Void in
                            if !(error != nil) {
                                marker.userData = UIImage(data:imageData!)
                            }
                            
                        })
                        marker.icon = UIImage(named: filter+"-icon")
                        marker.map = mapView
                }
            }
        }
        
    }
    
    
    
    func compressImage(original:UIImage, scale:CGFloat)->UIImage{
        let originalSize = original.size
        let newSize = CGSizeMake(originalSize.width * scale, originalSize.height * scale)
        UIGraphicsBeginImageContext(newSize)
        original.drawInRect(CGRectMake(0, 0, newSize.width, newSize.height))
        let compressedImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return compressedImage
        
    }
    
    func mapView(mapView: GMSMapView!, didTapInfoWindowOfMarker marker: GMSMarker!) {
        eventID = marker.snippet
        performSegueWithIdentifier("mapToEvent", sender: self)
    }
    
    func mapView(mapView: GMSMapView!, markerInfoWindow marker: GMSMarker!) -> UIView! {
        var infoWindow :CustomInfoWindow = NSBundle.mainBundle().loadNibNamed("eventInfo", owner: self, options: nil)[0] as! CustomInfoWindow
        grabEventData(marker.snippet)
        //Have eventid then pull what ever data that you need.
        infoWindow.eventTitle.text = marker.title
        infoWindow.eventDescription.text = eventDescription
        infoWindow.voteLabel.text = String(eventVote)
        // print( marker.valueForKeyPath("description") as? String)
        infoWindow.eventAttendees.text = String(eventAttendees)
        infoWindow.eventImage.image = marker.userData as? UIImage
        infoWindow.eventImage.layer.cornerRadius = 20
        infoWindow.eventImage.layer.masksToBounds = true
        
        return infoWindow
    }
    
    func grabEventData(id:String){
        let object : PFObject = PFObject(withoutDataWithClassName: "Events", objectId: id)
        eventDescription = object["eventDescription"] as! String
        eventVote = object["Likes"] as! Int
        eventAttendees = object["Joins"] as! Int
        
    }
    
    func mapView(mapView: GMSMapView!, didTapMarker marker: GMSMarker!) -> Bool {
        mapView.selectedMarker = marker
        return true
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func partyFilter(sender: AnyObject) {
        mapView.clear()
        
        partyButton.selected = !partyButton.selected.boolValue
        //I wanna call one function that filters the thing
        if(partyButton.selected.boolValue == true){
           setMarkerData("food", all: false)
        }else{
           setMarkerData("", all: true)
        }
    }
    
    @IBAction func sportsFilter(sender: AnyObject) {
        mapView.clear()
        sportsButton.selected = !sportsButton.selected.boolValue
        if(sportsButton.selected.boolValue == true){
            setMarkerData("sports", all: false)
        }else{
            setMarkerData("", all: true)
        }
    }
    
    @IBAction func musicFilter(sender: AnyObject) {
        mapView.clear()
        musicButton.selected = !musicButton.selected.boolValue
        if(musicButton.selected.boolValue == true){
            setMarkerData("music", all: false)
        }else{
            setMarkerData("", all: true)
        }
    }
    
    @IBAction func gameFilter(sender: AnyObject) {
        mapView.clear()
        gameButton.selected = !gameButton.selected.boolValue
        if(gameButton.selected.boolValue == true){
            setMarkerData("games", all: false)
        }else{
            setMarkerData("", all: true)
        }
        
    }
    
    @IBAction func outdoorsFilter(sender: AnyObject) {
        mapView.clear()
        outdoorsButton.selected = !outdoorsButton.selected.boolValue
        if(outdoorsButton.selected.boolValue == true){
            setMarkerData("trail", all: false)
        }else{
            setMarkerData("", all: true)
        }
    }
    
    @IBAction func otherFilter(sender: AnyObject) {
        mapView.clear()
        otherButton.selected = !otherButton.selected.boolValue
        if(otherButton.selected.boolValue == true){
            setMarkerData("other", all: false)
        }else{
            setMarkerData("", all: true)
        }
        
    }
    
    @IBAction func eduFilter(sender: AnyObject) {
        mapView.clear()
        otherButton.selected = !otherButton.selected.boolValue
        if(otherButton.selected.boolValue == true){
            setMarkerData("edu", all: false)
        }else{
            setMarkerData("", all: true)
        }
    }
    
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "mapToEvent"){
            let destinationViewController = segue.destinationViewController as! EventViewController
            
            destinationViewController.eventID = eventID
            //Pass event id here,  then use the event id to query what you need fam.
        }
    }

    
}
