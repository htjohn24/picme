//
//  LikeButton.h
//  Picme
//
//  Created by John Nguyen on 7/5/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class LikeButton;
@protocol LikeButtonDelegate
- (void) like:(LikeButton *)button didTapWithSectionIndex:(NSInteger)index;
@end


@interface LikeButton : UIButton

@property (nonatomic, assign) NSInteger sectionIndex;
@property (nonatomic, weak) id <LikeButtonDelegate> delegate;
@end
