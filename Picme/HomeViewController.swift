//
//  HomeViewController.swift
//  Picme
//
//  Created by John Nguyen on 9/9/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

protocol HomeViewDelegate {
    func updateToFeed(data : NSDictionary)
}

class HomeViewController: UIViewController , FilterViewDelegate {

    var delegate:HomeViewDelegate? = nil
    var filter = ""
    var internalData: NSDictionary = ["proximity": 50,"size" : 50,"date": 2 , "edu": true , "verified" : false]
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func updateData(data: NSDictionary) {
        internalData = data
       let proximity: Double = internalData["proximity"] as! Double
              println("Updating Data: " + String(stringInterpolationSegment: proximity))
       NSNotificationCenter.defaultCenter().postNotificationName("filterApply", object: nil, userInfo: internalData as [NSObject : AnyObject])
        
        
    }
    
    @IBAction func foodFilter(sender: AnyObject) {
        filter = "food"
        NSNotificationCenter.defaultCenter().postNotificationName("loadFood", object: nil)
        
    }
    
    @IBAction func sportsFilter(sender: AnyObject) {
        filter = "sports"
         NSNotificationCenter.defaultCenter().postNotificationName("loadSports", object: nil)
        
    }
    
    
    @IBAction func hikeFilter(sender: AnyObject) {
        filter = "trail"
         NSNotificationCenter.defaultCenter().postNotificationName("loadTrail", object: nil)
        
    }
    
    
    @IBAction func gameFilter(sender: AnyObject) {
        filter = "games"
        NSNotificationCenter.defaultCenter().postNotificationName("loadGames", object: nil)
        
    }
    
    @IBAction func musicFilter(sender: AnyObject) {
        filter = "music"
        NSNotificationCenter.defaultCenter().postNotificationName("loadMusic", object: nil)
    }
    
    @IBAction func eduFilter(sender: AnyObject) {
        filter = "edu"
        NSNotificationCenter.defaultCenter().postNotificationName("loadEdu", object: nil)
    }
    
    
    
    
    @IBAction func clearFilter(sender: AnyObject) {
         NSNotificationCenter.defaultCenter().postNotificationName("loadClear", object: nil)
    }
    
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "homeToFeed"){
            
            let destinationViewController = segue.destinationViewController as! EventFeedTableViewController
            destinationViewController.proximity = internalData["proximity"] as! Double
        }
        
        
        if(segue.identifier == "homeToFilter"){
              let nav = segue.destinationViewController as! UINavigationController
            let modalViewController :FilterViewController = nav.topViewController as! FilterViewController
            modalViewController.delegate = self
           modalViewController.promixity = internalData["proximity"] as! Int
           modalViewController.groupSize = internalData["size"] as! Int
           modalViewController.verifiedBool = internalData["verified"] as! Bool
           modalViewController.eduBool = internalData["edu"] as! Bool
        }
        
    }
}

