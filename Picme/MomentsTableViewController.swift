//
//  MomentsTableViewController.swift
//  Picme
//
//  Created by John Nguyen on 6/16/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class MomentsTableViewController: PFQueryTableViewController , LikeButtonDelegate {
    
    
    var eventId = ""
    var momentsId = ""
    var likeArray = NSMutableArray()
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        let query : PFQuery = PFQuery(className: "MomentLikes")
        
        
        query.whereKey("userID", equalTo: PFUser.currentUser()!)
        query.includeKey("momentID")
        query.findObjectsInBackgroundWithBlock ({ (activities, error) ->Void in
            if((error) == nil){
                if(activities?.count > 0){
                    for activity in activities!{
                        let event:PFObject = activity["momentID"] as! PFObject
                        self.likeArray.addObject(event.objectId!)
                        self.tableView.reloadData()
                    }
                }
                
            }else{
                println(error)
            }
        })
    }
    
   
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Configure the PFQueryTableView
        self.parseClassName = "EventPictures"
        
        self.pullToRefreshEnabled = true
        self.paginationEnabled = true
        self.objectsPerPage = 3
    }
    
    
    override func viewWillAppear(animated: Bool) {
        loadObjects()
        
    }
    
    
    override func objectsDidLoad(error: NSError!) {
        super.objectsDidLoad(error)
    }
    
    override func objectAtIndexPath(indexPath: NSIndexPath!) -> PFObject? {
        if(indexPath.section < self.objects!.count){
            return self.objects![indexPath.section] as? PFObject
        }else{
            return nil
        }
    }
    

    
    override func tableView(tableView: (UITableView!), cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject!) -> PFTableViewCell? {
        if(indexPath.section == self.objects!.count){
            var cell:LoadTableViewCell = self.tableView(tableView, cellForNextPageAtIndexPath: indexPath)! as! LoadTableViewCell
            return cell
        }else{
            let imageCell = tableView.dequeueReusableCellWithIdentifier("PhotoCell") as! BodyTableViewCell
            let user : PFUser = object["hostUser"] as! PFUser
             let profilePicture:PFFile = user["profileImage"] as! PFFile
            //            let eventImageView : PFImageView = imageCell.viewWithTag(1) as! PFImageView
            
           

            var tapEvent = UITapGestureRecognizer(target: self, action: "tappedEvent:")
            
            imageCell.profileImageView.file = profilePicture
            imageCell.eventImageView.file = object.objectForKey("FileName") as? PFFile
            imageCell.captionLabel.text = object["caption"] as? String
            imageCell.profileImageView.layer.cornerRadius = 20
            imageCell.profileImageView.layer.masksToBounds = true
            momentsId = object.objectId!
            imageCell.profileImageView.loadInBackground(nil)
            imageCell.eventImageView.loadInBackground(nil)
            
            
            return imageCell
        }
    }
    
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var sections:NSInteger = self.objects!.count
        if(self.paginationEnabled && sections > 0){
            sections++
        }else{
            return 1
        }
        return sections
        
    }
    
    override func _indexPathForPaginationCell() -> NSIndexPath {
        return NSIndexPath(forRow: 0, inSection: self.objects!.count)
    }
    
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == self.objects!.count){
            return 0
        }
        return 0
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == self.objects!.count){
            return 0
        }
        return 50.0
    }
    
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == self.objects!.count){
            return 50.0
        }
        return 320.0
    }
    
    
    
    override func tableView(tableView: UITableView, cellForNextPageAtIndexPath indexPath: (NSIndexPath!)) -> PFTableViewCell? {
        var loadMoreCell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell") as! PFTableViewCell
        return loadMoreCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == self.objects!.count && self.paginationEnabled){
            self.loadNextPage()
        }else{
            
        }
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = tableView.dequeueReusableCellWithIdentifier("FooterView") as! MomentsFooterTableViewCell
        let comments :PFQuery = PFQuery(className: "Comments")
        comments.whereKey("commentId", equalTo: momentsId)
        
        footerView.likeButton.delegate = self
        footerView.likeButton.sectionIndex = section
        
        let indexOfMatchedObject = self.likeArray.indexOfObject(momentsId)
        if(indexOfMatchedObject == NSNotFound){
            footerView.likeButton.selected = false
            println("Found")
        }else{
            println("Not Found")
            footerView.likeButton.selected = true
        }
        footerView.commentsLabel.text = String(comments.countObjects())
        return footerView
    }
    
    func like(button: LikeButton!, didTapWithSectionIndex index: Int) {
        let moments : PFObject = self.objects![index] as! PFObject
        let user : PFUser = PFUser.currentUser()!
        //Check if button is selected or not
        if(button.selected == true){
            self.unlikeEvent(moments , user: user)
            
        }else{
            self.likeEvent(moments , user: user)
        }
    }
    
    
    func likeEvent(moment:PFObject , user: PFUser){
        let activity : PFObject = PFObject(className: "MomentLikes")
        activity["momentID"] = moment
        activity["userID"] = user
        activity.save()
        
        var likes : Int = moment["Likes"] as! Int
        moment.setObject(likes + 1 , forKey: "Likes")
        moment.save()
        
    }
    
    
    func unlikeEvent(moment:PFObject , user: PFUser){
        let query :PFQuery = PFQuery(className: "MomentLikes")
        query.whereKey("userID", equalTo: user)
        query.whereKey("eventID", equalTo: moment)
        query.findObjectsInBackgroundWithBlock { (likes, error) -> Void in
            if(error == nil){
                for like in likes!{
                    like.delete()
                    self.likeArray.removeObject(moment.objectId!)
                    var likes : Int = moment["Likes"] as! Int
                    moment.setObject(likes - 1 , forKey: "Likes")
                    moment.save()
                }
            }
        }
    }
    
    
    override func queryForTable() -> PFQuery {
        let query: PFQuery = PFQuery(className: self.parseClassName!)
        query.includeKey("hostUser")
        query.whereKey("eventID", equalTo: eventId)
        query.orderByDescending("createdAt")
        
        //I call a query that takes in hostUser. Where is equal to the hostId.
        
        return query
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if(segue.identifier == "momentToSender"){
           
            let nav = segue.destinationViewController as! UINavigationController
            let destinationViewController = nav.topViewController as! CommentsViewController
            println(momentsId)
            destinationViewController.Id = momentsId
        }

    }
    
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
