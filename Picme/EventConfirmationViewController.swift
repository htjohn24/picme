//
//  EventConfirmationViewController.swift
//  Picme
//
//  Created by John Nguyen on 9/2/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit


class EventConfirmationViewController: UIViewController {

    var eventId = ""
    var eventTitle = "Churrios"
    var eventDescription = "Cherries are happy fruits"
    var eventAttendees = 0
    var eventLove = 0
    var eventShares = 0
    var eventID = "OB1WgiTrBS"
    var eventJoins = 0
    var eventHost = "Rich Del Ray"
    var eventLocation = "Seattle"
    var hostRating: Double = 0
    var eventDate = NSDate()
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var locationLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var eventImageView: PFImageView!
    @IBOutlet weak var profileImageView: PFImageView!
    @IBOutlet weak var eventAmountLabel: UILabel!
    @IBOutlet weak var eventLikesLabel: UILabel!
    @IBOutlet weak var ratingCosmos: CosmosView!
    @IBOutlet weak var hostLabel: UILabel!
    @IBOutlet weak var mapView: GMSMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let object : PFObject = PFObject(withoutDataWithClassName: "Events", objectId: eventId)
        eventTitle = object["eventTitle"] as! String
        eventDescription = object["eventDescription"] as! String
        eventLove = object["Likes"] as! Int
        eventJoins = object["Joins"] as! Int
        eventLocation = object["Address"] as! String
        eventDate = object["eventStartDate"] as! NSDate
        
        let host : PFObject = object["hostUser"] as! PFObject
        let hostName = host["username"] as! String
         let profilePicture:PFFile = host["profileImage"] as! PFFile
        hostRating = host["averageReview"] as! Double
        let eventPicture : PFFile = object["eventPicture"] as! PFFile
        let xyCoord : PFGeoPoint = object["xyCoord"] as! PFGeoPoint
        let position : CLLocationCoordinate2D = CLLocationCoordinate2DMake(xyCoord.latitude, xyCoord.longitude)
        mapView.camera = GMSCameraPosition(target: position, zoom: 15, bearing: 0, viewingAngle: 0.0)
        var marker = GMSMarker()
        marker.position = position
        marker.map = mapView
        
        
        let numberOfEvents : PFQuery = PFQuery(className: "Events")
         numberOfEvents.includeKey("hostUser")
        
         numberOfEvents.whereKey("hostUser", equalTo: PFUser.objectWithoutDataWithObjectId(host.objectId!))
        
        profileImageView.file = profilePicture
        eventImageView.file = eventPicture
        
        profileImageView.loadInBackground(nil)
        eventImageView.loadInBackground(nil)
        
        locationLabel.lineBreakMode = NSLineBreakMode.ByWordWrapping
        locationLabel.numberOfLines = 0
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.LongStyle
        dateFormatter.timeStyle = .ShortStyle
        dateFormatter.doesRelativeDateFormatting = true
        eventLikesLabel.text = String(eventLove)
        hostLabel.text = hostName
        locationLabel.text = eventLocation
        titleLabel.text = eventTitle
        descriptionLabel.text = eventDescription
        profileImageView.layer.cornerRadius = 50
        profileImageView.layer.masksToBounds = true
        eventAmountLabel.text = String(numberOfEvents.countObjects())
        ratingCosmos.rating = hostRating
        timeLabel.text = dateFormatter.stringFromDate(eventDate)

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func cancel(sender: AnyObject) {
      
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    @IBAction func accept(sender: AnyObject) {
        let object : PFObject = PFObject(withoutDataWithClassName: "Events", objectId: self.eventId)
        self.attendEvent(object, user: PFUser.currentUser()!)
        self.dismissViewControllerAnimated(true, completion: { () -> Void in
            //This is where I do the logic for join. 
           
        })
    }
    
    func attendEvent(event: PFObject , user: PFUser){
        let activity : PFObject = PFObject(className: "Joins")
        activity["eventID"] = event
        activity["userID"] = user
        activity.save()
        var joins : Int = event["Joins"] as! Int
        event.setObject(joins + 1 , forKey: "Joins")
        event.save()
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
