//
//  ShareButton.h
//  Picme
//
//  Created by John Nguyen on 8/26/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ShareButton;
@protocol ShareButtonDelegate
- (void) share:(ShareButton *)button didTapWithSectionIndex:(NSInteger)index;
@end


@interface ShareButton : UIButton

@property (nonatomic, assign) NSInteger sectionIndex;
@property (nonatomic, weak) id <ShareButtonDelegate> delegate;
@end
