//
//  SignupViewController.swift
//  Picme
//
//  Created by John Nguyen on 4/4/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit
import Parse

class SignupViewController: UIViewController {

    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    @IBOutlet weak var passwordCheck: UIImageView!
    @IBOutlet weak var usernameCheck: UIImageView!
    @IBOutlet weak var emailCheck: UIImageView!
    @IBOutlet weak var passwordConfirmCheck: UIImageView!
    
    var isValidUsername = false
    var isValidEmail = false
    var isValidPassword = false
    var isValidConfirmPassword = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }

    @IBAction func usernameCheck(sender: AnyObject) {
        let regex = "^[a-z0-9_-]{3,16}"
        if(isValid(usernameTextField.text, regex: regex)){
            println("Valid")
            usernameCheck.alpha = 1
            isValidUsername = true
        }else{
            println("Not Valid")
            isValidUsername = false
        }
        
    }
    
    @IBAction func emailCheck(sender: AnyObject) {
        let regex = "^\\w+@[a-zA-Z_]+?\\.edu"
        if(isValid(emailTextField.text, regex: regex)){
            println("Valid")
            emailCheck.alpha = 1
            isValidEmail = true
            
        }else{
            println("Not Valid")
            emailCheck.alpha = 0.5
            isValidEmail = false
        }
        
    }
    
    
    @IBAction func passwordCheck(sender: AnyObject) {
        if(passwordTextField.text != ""){
            println("Valid")
            passwordCheck.alpha = 1
            isValidPassword = true
            
        }else{
            println("Not Valid")
            passwordCheck.alpha = 0.5
            isValidPassword = false
        }
    }
    
    
    @IBAction func passwordConfirmedCheck(sender: AnyObject) {
        if(passwordTextField.text == confirmPasswordTextField.text){
            println("Valid")
            passwordConfirmCheck.alpha = 1
            isValidConfirmPassword = true

        }else{
            println("Not Valid")
            passwordConfirmCheck.alpha = 0.5
            isValidConfirmPassword = false
        }
    }
 
    
    @IBAction func signup(sender: AnyObject) {
        print("Sign up")
        if(isValidConfirmPassword && isValidUsername && isValidPassword && isValidConfirmPassword){
            var profileImage = UIImage(named: "profile")
            var profileBackground = UIImage(named: "profileBackground")
            let profileData: NSData = UIImagePNGRepresentation(profileImage)
            let backgroundData: NSData = UIImagePNGRepresentation(profileBackground)
            let profileFile : PFFile = PFFile(data: profileData)
            let backgroundFile : PFFile = PFFile(data: backgroundData)
            var user = PFUser()
            
            user.username = usernameTextField.text
            user.password = passwordTextField.text.MD5()
            user.email = emailTextField.text
            user["profileImage"] = profileFile
            user["profileBackground"] = backgroundFile
            user.signUpInBackgroundWithBlock({ (success, error) -> Void in
                if(success){
                    self.dismissViewControllerAnimated(true, completion: nil)
                }
            })
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func isValid(testStr:String, regex: String) -> Bool {
        // println("validate calendar: \(testStr)")
        let emailRegEx = regex
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluateWithObject(testStr)
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true);
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
