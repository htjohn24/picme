//
//  BodyTableViewCell.swift
//  Picme
//
//  Created by John Nguyen on 4/5/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class BodyTableViewCell: PFTableViewCell {

    @IBOutlet weak var eventImageView: PFImageView!
    @IBOutlet weak var profileImageView: PFImageView!
    @IBOutlet weak var captionLabel: KILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
