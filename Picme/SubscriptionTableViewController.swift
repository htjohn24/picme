//
//  FeedTableViewController.swift
//  Picme
//
//  Created by John Nguyen on 4/5/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class SubscriptionTableViewController:PFQueryTableViewController, UIGestureRecognizerDelegate, LikeButtonDelegate  , AttendButtonDelegate{
    
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var eventId = "Uj3eSI72DO"
    var hostId = ""
    var index = 10
    var likeArray = NSMutableArray()
    var attendArray = NSMutableArray()
    var isActive = false
    var commentCount = 0
    var isLike = false
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.parseClassName = "Events"
        
        self.pullToRefreshEnabled = true
        self.paginationEnabled = true
        self.objectsPerPage = 20
        
    }
    
    override func viewWillAppear(animated: Bool) {
        let currentUser = PFUser.currentUser()
        if(currentUser != nil){
            
        }else{
            self.performSegueWithIdentifier("feedToLogin", sender: self)
        }
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        loadObjects()
        //Make a timer here
        let time : NSTimer = NSTimer(timeInterval: 2, target: self , selector: Selector("DataReload"), userInfo: nil, repeats: false)
    }
    
    func DataReload(){
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func objectsDidLoad(error: NSError!) {
        super.objectsDidLoad(error)
        if(PFUser.currentUser() != nil){
            let query : PFQuery = PFQuery(className: "Likes")
            
            
            query.whereKey("userID", equalTo: PFUser.currentUser()!)
            println(PFUser.currentUser()?.username)
            query.includeKey("eventID")
            query.findObjectsInBackgroundWithBlock { (activities, error) ->Void in
                if((error) == nil){
                    if(activities?.count > 0){
                        for activity in activities!{
                            let event:PFObject = activity["eventID"] as! PFObject
                            self.likeArray.addObject(event.objectId!)
                            println(activity)
                            
                            //Learn ways of how to implement this
                        }
                    }
                    self.tableView.reloadData()
                }else{
                    println(error)
                }
            }
            
            let queryTwo : PFQuery = PFQuery(className: "Joins")
            queryTwo.whereKey("userID", equalTo: PFUser.currentUser()!)
            queryTwo.includeKey("eventID")
            queryTwo.findObjectsInBackgroundWithBlock { (activities, error) ->Void in
                if((error) == nil){
                    if(activities?.count > 0){
                        for activity in activities!{
                            let event:PFObject = activity["eventID"] as! PFObject
                            self.attendArray.addObject(event.objectId!)
                            println(activity)
                        }
                    }
                    self.tableView.reloadData()
                }else{
                    println(error)
                }
            }
        }
    }
    
    override func objectAtIndexPath(indexPath: NSIndexPath!) -> PFObject? {
        if(indexPath.section < self.objects!.count){
            return self.objects![indexPath.section] as? PFObject
        }else{
            return nil
        }
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section == self.objects!.count){
            return nil
        }
        
        let sectionHeaderView : HeaderTableViewCell = tableView.dequeueReusableCellWithIdentifier("SectionHeaderCell") as! HeaderTableViewCell
        
        let profileImageView : PFImageView = sectionHeaderView.viewWithTag(1) as! PFImageView
        let usernameLabel : UILabel = sectionHeaderView.viewWithTag(2) as! UILabel
        let descriptionLabel : KILabel = sectionHeaderView.viewWithTag(3) as! KILabel
        let voteLabel : UILabel = sectionHeaderView.viewWithTag(4) as! UILabel
        
        sectionHeaderView.like.sectionIndex = section
        var tapProfile = UITapGestureRecognizer(target: self, action: "tappedProfile:")
        tapProfile.numberOfTapsRequired = 1
        profileImageView.userInteractionEnabled = true
        profileImageView.tag = section
        profileImageView.addGestureRecognizer(tapProfile)
        
        descriptionLabel.systemURLStyle = true
        descriptionLabel.hashtagLinkTapHandler = { label, hashtag, range in
            let alert : UIAlertController = UIAlertController(title: "Tapped" + hashtag, message: "Tapped" + hashtag, preferredStyle: UIAlertControllerStyle.Alert)
            NSLog("Hashtah \(hashtag) tapped")
        }
        
        
        let event : PFObject = self.objects![section] as! PFObject
        let user : PFUser = event["hostUser"] as! PFUser
        let title : NSString = event["eventTitle"] as! NSString
        let profilePicture:PFFile = user["profileImage"] as! PFFile
        let vote : NSInteger = event["Likes"] as! NSInteger
        let description : NSString = event["eventDescription"] as! NSString
        let hostID : String = user.objectId!
        
        hostId = hostID
        println(event.objectId)
        let indexOfMatchedObject = self.likeArray.indexOfObject(event.objectId!)
        if(indexOfMatchedObject == NSNotFound){
            sectionHeaderView.like.selected = false
        }else{
            sectionHeaderView.like.selected = true
        }
        sectionHeaderView.like.delegate = self
        sectionHeaderView.like.sectionIndex = section
        
        usernameLabel.text = title as String
        descriptionLabel.text = description as String
        profileImageView.file = profilePicture
        voteLabel.text = String(vote)
        profileImageView.layer.cornerRadius = 20
        profileImageView.layer.masksToBounds = true
        profileImageView.loadInBackground(nil)
        
        return sectionHeaderView
    }
    
    func tappedProfile(sender : UITapGestureRecognizer){
        self.performSegueWithIdentifier("feedToProfile", sender: self)
    }
    
    
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: (UITableView!), cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject!) -> PFTableViewCell? {
        if(indexPath.section == self.objects!.count){
            var cell:LoadTableViewCell = self.tableView(tableView, cellForNextPageAtIndexPath: indexPath)! as! LoadTableViewCell
            return cell
        }else{
            let imageCell = tableView.dequeueReusableCellWithIdentifier("PhotoCell") as! BodyTableViewCell
            
            var tapEvent = UITapGestureRecognizer(target: self, action: "tappedEvent:")
            eventId = object.objectId!
            imageCell.eventImageView.file = object.objectForKey("eventPicture") as? PFFile
            
            
            
            imageCell.eventImageView.loadInBackground({ (image, error) -> Void in
                
            })
            return imageCell
        }
    }
    
    func tappedEvent(sender : UITapGestureRecognizer){
        self.performSegueWithIdentifier("feedToEvent", sender: self)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var sections:NSInteger = self.objects!.count
        if(self.paginationEnabled && sections > 0){
            sections++
        }
        return sections
    }
    
    override func _indexPathForPaginationCell() -> NSIndexPath {
        return NSIndexPath(forRow: 0, inSection: self.objects!.count)
    }
    
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == self.objects!.count){
            return 0
        }
        return 50.0
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == self.objects!.count){
            return 0
        }
        return 50.0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == self.objects!.count){
            return 50.0
        }
        return 320.0
    }
    
    override func tableView(tableView: UITableView, cellForNextPageAtIndexPath indexPath: (NSIndexPath!)) -> PFTableViewCell? {
        var loadMoreCell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell") as! PFTableViewCell
        return loadMoreCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == self.objects!.count && self.paginationEnabled){
            self.loadNextPage()
        }else{
            let object :PFObject = (objects![indexPath.section] as! PFObject)
            eventId = object.objectId!
            
            self.performSegueWithIdentifier("feedToEvent", sender: self)
        }
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = tableView.dequeueReusableCellWithIdentifier("FooterView") as! FooterTableViewCell
        footerView.attend.delegate = self
        footerView.attend.sectionIndex = section
        let event : PFObject = self.objects![section] as! PFObject
        
        
        let indexOfMatchedObject = self.attendArray.indexOfObject(event.objectId!)
        if(indexOfMatchedObject == NSNotFound){
            footerView.attend.selected = false
        }else{
            footerView.attend.selected = true
        }
        
        return footerView
    }
    
    @IBAction func addEvent(sender: AnyObject) {
        self.performSegueWithIdentifier("feedToCamera", sender: self)
    }
    
    override func queryForTable() -> PFQuery {
        let followingQuery: PFQuery = PFQuery(className: "Subscriptions" )
        followingQuery.includeKey("hostUser")
        followingQuery.whereKey("fromUser", equalTo: PFUser.currentUser()!)
        followingQuery.orderByDescending("createdAt")
        
        let eventsFromFollowedUsersQuery : PFQuery =  PFQuery(className: self.parseClassName!)
        eventsFromFollowedUsersQuery.whereKey("hostUser", matchesKey: "toUser", inQuery: followingQuery)
        
        let eventsFromCurrentUserQuery : PFQuery = PFQuery(className : self.parseClassName!)
        eventsFromCurrentUserQuery.whereKey("hostUser", equalTo: PFUser.currentUser()!)
        
        let superQuery = PFQuery.orQueryWithSubqueries([ eventsFromCurrentUserQuery, eventsFromFollowedUsersQuery])
        return superQuery
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "feedToEvent"){
            let destinationViewController = segue.destinationViewController as! EventViewController
            println("feedToEvent:" + hostId)
            destinationViewController.eventID = eventId
        }
        
        if(segue.identifier == "feedToProfile"){
            let destinationViewController = segue.destinationViewController as! ProfileViewController
            
            destinationViewController.profileId = hostId
        }
        
        if(segue.identifier == "eventToSender"){
            print("Loaded table up")
            let nav = segue.destinationViewController as! UINavigationController
            let destinationViewController = nav.topViewController as! CommentsViewController
            println("Event Id: " + eventId)
            destinationViewController.Id = eventId
        }
    }
    
    
    func like(button: LikeButton!, didTapWithSectionIndex index: Int) {
        let event : PFObject = self.objects![index] as! PFObject
        let user : PFUser = PFUser.currentUser()!
        println("tapped")
        //Check if button is selected or not
        if(button.selected == true){
            self.unlikeEvent(event , user: user)
            isLike = false
        }else{
            self.likeEvent(event , user: user)
            isLike = true
            
        }
    }
    
    func attend(button: AttendButton!, didTapWithSectionIndex index: Int) {
        let event: PFObject = self.objects![index] as! PFObject
        let user : PFUser = PFUser.currentUser()!
        
        if(button.selected == true){
            self.unattendEvent(event, user: user)
        }else{
            self.attendEvent(event, user: user)
        }
    }
    
    func attendEvent(event: PFObject , user: PFUser){
        let activity : PFObject = PFObject(className: "Joins")
        activity["eventID"] = event
        activity["userID"] = user
        activity.save()
    }
    
    func unattendEvent(event : PFObject , user: PFUser){
        let query :PFQuery = PFQuery(className: "Likes")
        query.whereKey("userID", equalTo: user)
        query.whereKey("eventID", equalTo: event)
        query.findObjectsInBackgroundWithBlock { (likes, error) -> Void in
            if(error == nil){
                for like in likes!{
                    like.delete()
                }
            }
        }
    }
    
    func likeEvent(event:PFObject , user: PFUser){
        println("liked event")
        let activity : PFObject = PFObject(className: "Likes")
        activity["eventID"] = event
        activity["userID"] = user
        activity.save()
        
        if(isActive == false){
            isActive = true
            print("isNotActive")
            self.update(event)
        }
    }
    
    
    func unlikeEvent(event:PFObject , user: PFUser){
        println("unliked event")
        let query :PFQuery = PFQuery(className: "Likes")
        query.whereKey("userID", equalTo: user)
        query.whereKey("eventID", equalTo: event)
        query.findObjectsInBackgroundWithBlock { (likes, error) -> Void in
            if(error == nil){
                for like in likes!{
                    like.delete()
                }
                if(self.isActive == false){
                    self.isActive = true
                    println("isActive")
                    self.update(event)
                }
            }
        }
    }
    
    func update(events: PFObject) {
        println("Inside of update block")
        let query: PFQuery = PFQuery(className: "Events")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        let event : PFObject = events as PFObject
        println("ObjectId: " + event.objectId!)
        let likes = event["Likes"] as! NSInteger
        query.getObjectInBackgroundWithId(event.objectId!, block: { (events, error) -> Void in
            if(error == nil){
                if(self.isLike == true){
                    println("Liked")
                    
                    events?.setObject(likes + 1 , forKey: "Likes")
                    events?.save()
                    self.isActive == false
                }
                if(self.isLike == false){
                    println("Disliked")
                    events?.setObject(likes - 1 , forKey: "Likes")
                    events?.save()
                    self.isActive == false
                }
            }else{
                
            }
        })
    }
    
    
    func joinedEvent(event:PFObject , user: PFUser){
        
    }
    
}
