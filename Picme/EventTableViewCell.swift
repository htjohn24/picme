//
//  EventTableViewCell.swift
//  Picme
//
//  Created by John Nguyen on 9/1/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class EventTableViewCell: PFTableViewCell {
    
    @IBOutlet weak var profileImageView: PFImageView!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var hostNameLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var attendButton: AttendButton!
    @IBOutlet weak var userReview: CosmosView!
    @IBOutlet weak var distanceLabel: UILabel!
    
    @IBOutlet weak var groupSizeLabel: UILabel!
    
    @IBOutlet weak var joinsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    @IBAction func attendButton(sender: AnyObject) {

        var joinsInt : Int = (joinsLabel.text?.toInt() as Int?)!
            println("Clicked : " + String(stringInterpolationSegment: attendButton.selected.boolValue))
        if(attendButton.selected == false){
            joinsLabel.text = String(joinsInt - 1)
        }else{
            
        }
        
        
    }
    
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
}
