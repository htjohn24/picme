//
//  ProfileButton.h
//  Picme
//
//  Created by John Nguyen on 8/26/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ProfileButton;
@protocol ProfileButtonDelegate
- (void) profile:(ProfileButton *)button didTapWithSectionIndex:(NSInteger)index;
@end


@interface ProfileButton : UIButton

@property (nonatomic, assign) NSInteger sectionIndex;
@property (nonatomic, weak) id <ProfileButtonDelegate> delegate;
@end
