//
//  CameraViewController.swift
//  Picme
//
//  Created by John Nguyen on 4/18/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit
import MobileCoreServices
import Parse
import MapKit

class CameraViewController: UIViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate , CLLocationManagerDelegate  , HSDatePickerViewControllerDelegate , NSURLConnectionDataDelegate{
    
     var placesClient: GMSPlacesClient?
    @IBOutlet weak var startDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextField: UITextField!
    @IBOutlet weak var chosenImageView: UIImageView!
    @IBOutlet weak var mapView: MKMapView!
    
    @IBOutlet weak var locationTextField: AutoCompleteTextField!
    
    @IBOutlet weak var foodButton: UIButton!
    @IBOutlet weak var sportsButton: UIButton!
    @IBOutlet weak var trailButton: UIButton!
    @IBOutlet weak var gamesButton: UIButton!
    @IBOutlet weak var musicButton: UIButton!
    @IBOutlet weak var eduButton: UIButton!
    
    
    private var responseData:NSMutableData?
    private var selectedPointAnnotation:MKPointAnnotation?
    private var connection:NSURLConnection?
    
    private let googleMapsKey = "AIzaSyD8-OfZ21X2QLS1xLzu1CLCfPVmGtch7lo"
    private let baseURLString = "https://maps.googleapis.com/maps/api/place/autocomplete/json"
    
    @IBOutlet weak var groupSizeLabel: UILabel!
    
    
    var datePicker = HSDatePickerViewController()
    let locationManager =  CLLocationManager()
    let imagePicker = UIImagePickerController()
    var startDate = NSDate(timeIntervalSinceNow:0)
    var endDate = NSDate(timeIntervalSinceNow: 60*60*24)
    var startDateChosen = false
    var endDateChosen = false
    var location = CLLocationCoordinate2D()
    var votes = 0
    var groupSize = 5
    var eventType = "Type"
   
    override func viewDidLoad() {
        super.viewDidLoad()
        print(startDate)
        placesClient = GMSPlacesClient()
        placeAutocomplete()
        configureTextField()
        handleTextFieldInterfaces()
        // Do any additional setup after loading the view.
    }
    
    private func configureTextField(){
        locationTextField.autoCompleteTextColor = UIColor(red: 128.0/255.0, green: 128.0/255.0, blue: 128.0/255.0, alpha: 1.0)
        locationTextField.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        locationTextField.autoCompleteCellHeight = 35.0
        locationTextField.maximumAutoCompleteCount = 20
        locationTextField.hidesWhenSelected = true
        locationTextField.hidesWhenEmpty = true
        locationTextField.enableAttributedText = true
        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName] = UIColor.blackColor()
        attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
        locationTextField.autoCompleteAttributes = attributes
    }
    
    private func handleTextFieldInterfaces(){
        locationTextField.onTextChange = {[weak self] text in
            if !text.isEmpty{
                if self!.connection != nil{
                    self!.connection!.cancel()
                    self!.connection = nil
                }
                let urlString = "\(self!.baseURLString)?key=\(self!.googleMapsKey)&input=\(text)"
                let url = NSURL(string: urlString.stringByAddingPercentEscapesUsingEncoding(NSASCIIStringEncoding)!)
                if url != nil{
                    let urlRequest = NSURLRequest(URL: url!)
                    self!.connection = NSURLConnection(request: urlRequest, delegate: self)
                }
            }
        }
        
        locationTextField.onSelect = {[weak self] text, indexpath in
            Location.geocodeAddressString(text, completion: { (placemark, error) -> Void in
                if placemark != nil{
                    let coordinate = placemark!.location.coordinate
                    //self!.addAnnotation(coordinate, address: text)
                    self!.locationTextField.text = text
                    self!.mapView.setCenterCoordinate(coordinate, zoomLevel: 12, animated: true)
                }
            })
        }
    }
    
    //MARK: NSURLConnectionDelegate
    func connection(connection: NSURLConnection, didReceiveResponse response: NSURLResponse) {
        responseData = NSMutableData()
    }
    
    func connection(connection: NSURLConnection, didReceiveData data: NSData) {
        responseData?.appendData(data)
    }
    
    func connectionDidFinishLoading(connection: NSURLConnection) {
        if responseData != nil{
            var error:NSError?
            if let result = NSJSONSerialization.JSONObjectWithData(responseData!, options: nil, error: &error) as? NSDictionary{
                let status = result["status"] as? String
                if status == "OK"{
                    if let predictions = result["predictions"] as? NSArray{
                        var locations = [String]()
                        for dict in predictions as! [NSDictionary]{
                            locations.append(dict["description"] as! String)
                        }
                        self.locationTextField.autoCompleteStrings = locations
                    }
                }
                else{
                    self.locationTextField.autoCompleteStrings = nil
                }
            }
        }
    }
    
    @IBAction func sizeSlider(sender: UISlider) {
        groupSize = Int(sender.value)
        groupSizeLabel.text = String(self.groupSize)
    }
    
    
    func connection(connection: NSURLConnection, didFailWithError error: NSError) {
        println("Error: \(error.localizedDescription)")
    }
    
    private func addAnnotation(coordinate:CLLocationCoordinate2D, address:String?){
        if selectedPointAnnotation != nil{
            mapView.removeAnnotation(selectedPointAnnotation)
        }
        
        selectedPointAnnotation = MKPointAnnotation()
        selectedPointAnnotation?.coordinate = coordinate
        selectedPointAnnotation?.title = address
        mapView.addAnnotation(selectedPointAnnotation)
    }
    
    override func viewWillAppear(animated: Bool) {
        initializeDate()
        super.viewWillAppear(animated)
        let initialLocation = CLLocation(latitude: 21.282778, longitude: -157.829444)
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        
        chosenImageView.userInteractionEnabled = true
        var tapGestureRecognizer = UITapGestureRecognizer(target: self, action: "tappedImage:")
        tapGestureRecognizer.numberOfTapsRequired = 1
        chosenImageView.addGestureRecognizer(tapGestureRecognizer)
        
        self.imagePicker.mediaTypes = NSArray(objects: kUTTypeImage) as [AnyObject]
        
    }
    
    func tappedImage(sender : UITapGestureRecognizer){
        var alert:UIAlertController=UIAlertController(title: "Choose Image", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        var cameraAction = UIAlertAction(title: "Camera", style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.openCamera()
                
        }
        var gallaryAction = UIAlertAction(title: "Gallary", style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.openGallary()
        }
        var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
            {
                UIAlertAction in
                
        }
        // Add the actions
        alert.addAction(cameraAction)
        alert.addAction(gallaryAction)
        alert.addAction(cancelAction)
        // Present the actionsheet
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
        
        
    }
    
    func openCamera()
    {
        if(UIImagePickerController .isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera))
        {
            imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        else
        {
            openGallary()
        }
    }
    func openGallary()
    {
        imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(imagePicker, animated: true, completion: nil)
        }
        
    }
    
    let regionRadius: CLLocationDistance = 1000
    func centerMapOnLocation(location: CLLocation) {
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(location.coordinate,
            regionRadius * 2.0, regionRadius * 2.0)
        mapView.setRegion(coordinateRegion, animated: true)
    }
    
    
    func locationManager(manager:CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus){
        if status == .AuthorizedWhenInUse{
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(manager:CLLocationManager!, didUpdateLocations locations:[AnyObject]!){
        if let location = locations.first as? CLLocation{
            
            let location = locations.last as! CLLocation
            
            let center = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            let region = MKCoordinateRegion(center: center, span: MKCoordinateSpan(latitudeDelta: 0.01, longitudeDelta: 0.01))
            
            self.mapView.setRegion(region, animated: true)
            locationManager.stopUpdatingLocation()
        }
    }
    
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        var chosenImage:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
        self.chosenImageView.image = chosenImage;
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    override func viewWillDisappear(animated: Bool) {
        self.clear()
    }
    
    func hsDatePickerWillDismissWithQuitMethod(method: HSDatePickerQuitMethod) {
        self.datePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    
    func getLocation(searchText : String ){
        let geocoder : CLGeocoder = CLGeocoder()
        geocoder.geocodeAddressString(searchText, completionHandler: { (placemarks, NSError) -> Void in
            var placeMark : CLPlacemark = placemarks[0] as! CLPlacemark
            var region : MKCoordinateRegion = MKCoordinateRegion()
            region.center.latitude = placeMark.location.coordinate.latitude
            region.center.longitude = placeMark.location.coordinate.longitude
            
            self.mapView.setRegion(region, animated: true)
            
        })
        
    }
    
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func touchesBegan(touches: Set<NSObject>, withEvent event: UIEvent) {
        self.view.endEditing(true);
    }
    
    
    func clear(){
        //self.titleTextField.text = nil
    }
    
    
    @IBAction func startDateButton(sender: AnyObject) {
        self.view.endEditing(true);
        startDateChosen = true
        datePicker.delegate = self
        datePicker.date = startDate
        self.presentViewController(datePicker, animated: true, completion: nil)
        
    }
    
    
    
    func refreshTitle(date : NSDate){
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd"
        
        if(startDateChosen == true){
            
            startDate = date
            var startDateString = dateFormatter.stringFromDate(startDate)
            startDateTextField.text = startDateString
            startDateChosen = false
        }
        if(endDateChosen == true){
            
            endDate = date
            var endDateString = dateFormatter.stringFromDate(endDate)
            endDateTextField.text = endDateString
            endDateChosen = false
        }
        
        
    }
    
    
    
    @IBAction func endDateButton(sender: AnyObject) {
        self.view.endEditing(true);
        endDateChosen = true
        datePicker.delegate = self
        datePicker.date = startDate
        self.presentViewController(datePicker, animated: true, completion: nil)
        
        
    }
    
    
    @IBAction func addEvent(sender: AnyObject) {
        if((chosenImageView.image) != nil){
            var event : PFObject = PFObject(className: "Events")
            var eventPictures : PFObject = PFObject(className: "EventPictures")
            let imageData : NSData = UIImagePNGRepresentation(scaleImage(chosenImageView.image!, newSize: CGSizeMake(222, 222) )) as NSData
            let photoFile : PFFile = PFFile(data: imageData)
            event["eventTitle"] = titleTextField.text as String
            event["eventDescription"] = descriptionTextField.text as String
            event["eventType"] = eventType as String
            event["xyCoord"] = PFGeoPoint(latitude: mapView.centerCoordinate.latitude, longitude: mapView.centerCoordinate.longitude)
            event["hostUser"] = PFUser.currentUser()
            event["eventPicture"] = photoFile
            event["rank"] = votes as NSInteger
            event["Likes"] = votes as NSInteger
            event["Joins"] = votes as NSInteger
            event["eventStartDate"] = startDate
            event["eventEndDate"] = endDate
            event["Address"] = locationTextField.text
            event["size"] = groupSize as NSInteger
            event.saveInBackgroundWithBlock {  (success, error) -> Void in
                if success {
                    NSLog("Object created with id: (gameScore.objectId)")
                    eventPictures["FileName"] = photoFile
                    eventPictures["eventID"] = event.objectId
                    eventPictures["hostUser"] = PFUser.currentUser()
                    eventPictures["caption"] = self.descriptionTextField.text as String
                    eventPictures.saveInBackgroundWithBlock({ (success, error) -> Void in
                    
                    })
                    
                } else {
                    NSLog("%@", error!)
                }
                
            }
        }else{
            
        }
        //self.performSegueWithIdentifier("cameraToFeed", sender: self)
        self.tabBarController?.selectedIndex = 0
    }
    
    @IBAction func searchLocation(sender: AnyObject) {
        getLocation(locationTextField.text)
    }
    
    func initializeDate(){
        
        var dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM dd"
        var startDateString = dateFormatter.stringFromDate(startDate)
        var endDateString = dateFormatter.stringFromDate(endDate)
        startDateTextField.text = startDateString
        endDateTextField.text = endDateString
    }
    
    
    @IBAction func cancelEvent(sender: AnyObject) {
//        self.performSegueWithIdentifier("cameraToFeed", sender: self)
        self.tabBarController?.selectedIndex = 0
    }
    
    //by Lyndsay Scott
    func scaleImage(image: UIImage, newSize: CGSize) -> UIImage {
        
        var scaledSize = newSize
        var scaleFactor: CGFloat = 1.0
        
        if image.size.width > image.size.height {
            scaleFactor = image.size.width / image.size.height
            scaledSize.width = newSize.width
            scaledSize.height = newSize.width / scaleFactor
        } else {
            scaleFactor = image.size.height / image.size.width
            scaledSize.height = newSize.height
            scaledSize.width = newSize.width / scaleFactor
        }
        
        UIGraphicsBeginImageContextWithOptions(scaledSize, false, 0.0)
        let scaledImageRect = CGRectMake(0.0, 0.0, scaledSize.width, scaledSize.height)
        [image .drawInRect(scaledImageRect)]
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    
    
    func hsDatePickerDidDismissWithQuitMethod(method: HSDatePickerQuitMethod) {
        self.datePicker.dismissViewControllerAnimated(true, completion: nil)
    }
    
    func hsDatePickerPickedDate(date: NSDate!) {
        refreshTitle(date)
        print("Refreshed")
    }
    
    func placeAutocomplete() {
        let filter = GMSAutocompleteFilter()
        filter.type = GMSPlacesAutocompleteTypeFilter.City
        placesClient?.autocompleteQuery("Sydney Oper", bounds: nil, filter: filter, callback: { (results, error: NSError?) -> Void in
            if let error = error {
                println("Autocomplete error \(error)")
            }
            
            for result in results! {
                if let result = result as? GMSAutocompletePrediction {
                    println("Result \(result.attributedFullText) with placeID \(result.placeID)")
                }
            }
        })
    }
    
    
    @IBAction func setTypeFood(sender: AnyObject) {
        eventType = "food"
        foodButton.selected = true
        sportsButton.selected = false
        trailButton.selected = false
        gamesButton.selected = false
        musicButton.selected = false
        eduButton.selected = false

    }
    
    @IBAction func setTypeSports(sender: AnyObject) {
        eventType = "sports"
        foodButton.selected = false
        sportsButton.selected = true
        trailButton.selected = false
        gamesButton.selected = false
        musicButton.selected = false
        eduButton.selected = false
    }
    
    @IBAction func setTypeTrail(sender: AnyObject) {
        eventType = "trail"
        foodButton.selected = false
        sportsButton.selected = false
        trailButton.selected = true
        gamesButton.selected = false
        musicButton.selected = false
        eduButton.selected = false
    }
    
    @IBAction func setTypeGames(sender: AnyObject) {
        eventType = "games"
        foodButton.selected = false
        sportsButton.selected = false
        trailButton.selected = false
        gamesButton.selected = true
        musicButton.selected = false
        eduButton.selected = false
    }
    
    @IBAction func setTypeMusic(sender: AnyObject) {
        eventType = "music"
        foodButton.selected = false
        sportsButton.selected = false
        trailButton.selected = false
        gamesButton.selected = false
        musicButton.selected = true
        eduButton.selected = false
        print("Event Type: " + eventType)
    }
    
    @IBAction func setTypeEdu(sender: AnyObject) {
        eventType = "edu"
        foodButton.selected = false
        sportsButton.selected = false
        trailButton.selected = false
        gamesButton.selected = false
        musicButton.selected = false
        eduButton.selected = true
    }
    
    
    
    
    
    
    
    
    
    
}
