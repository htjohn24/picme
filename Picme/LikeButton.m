//
//  LikeButton.m
//  Picme
//
//  Created by John Nguyen on 7/5/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

#import "LikeButton.h"

@implementation LikeButton

/*
 - (id)initWithFrame:(CGRect)frame
 {
 self = [super initWithFrame:frame];
 if (self) {
 // Initialization code
 }
 return self;
 }*/

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super initWithCoder:aDecoder]) {
        [self addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void) buttonPressed {
    [self.delegate like:self didTapWithSectionIndex:self.sectionIndex];
}

@end
