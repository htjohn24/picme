//
//  MomentsFooterTableViewCell.swift
//  Picme
//
//  Created by John Nguyen on 8/15/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class MomentsFooterTableViewCell: UITableViewCell {

    @IBOutlet weak var likeButton: LikeButton!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var commentsLabel: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    @IBAction func clickLike(sender: AnyObject) {
        likeButton.selected = !likeButton.selected.boolValue
        let number:Int? = numberOfLikes.text?.toInt() as Int?
        
        if(likeButton.selected == true ){
            numberOfLikes.text = String(number! + 1)
        }else{
            numberOfLikes.text  = String(number! - 1 )
        }
        
    }
    
    
    

}
