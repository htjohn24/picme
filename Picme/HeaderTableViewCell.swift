//
//  HeaderTableViewCell.swift
//  Picme
//
//  Created by John Nguyen on 5/24/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class HeaderTableViewCell: PFTableViewCell {
    
    
    @IBOutlet weak var like: LikeButton!
    @IBOutlet weak var numberOfLikes: UILabel!
    @IBOutlet weak var profile: ProfileButton!

    override func awakeFromNib() {
        super.awakeFromNib()
        
       
        // Initialization code
    }
    
    @IBAction func clickLike(sender: AnyObject) {
        like.selected = !like.selected.boolValue
        let number:Int? = numberOfLikes.text?.toInt() as Int?
        
        if(like.selected == true ){
            numberOfLikes.text = String(number! + 1)
        }else{
            numberOfLikes.text  = String(number! - 1 )
        }
    }
    
   
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    

}
