//
//  CommentsTableViewController.swift
//  Picme
//
//  Created by John Nguyen on 6/18/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class CommentsTableViewController: PFQueryTableViewController {
    
    var Id = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadList:",name:"load", object: nil)
        // Do any additional setup after loading the view.
    }
    
    func loadList(notification: NSNotification){
        loadObjects()
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        // Configure the PFQueryTableView
        self.parseClassName = "Comments"
        self.pullToRefreshEnabled = true
        self.paginationEnabled = true
        self.objectsPerPage = 10
    }
    
    
    override func viewWillAppear(animated: Bool) {
        loadObjects()
    }
    
    
    override func objectsDidLoad(error: NSError!) {
        super.objectsDidLoad(error)
    }
    
    override func objectAtIndexPath(indexPath: NSIndexPath!) -> PFObject? {
        if(indexPath.section < self.objects!.count){
            return self.objects![indexPath.section] as? PFObject
        }else{
            return nil
        }
    }
    
    override func tableView(tableView: (UITableView!), cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject!) -> PFTableViewCell? {
        if(indexPath.section == self.objects!.count){
            var cell:LoadTableViewCell = self.tableView(tableView, cellForNextPageAtIndexPath: indexPath)! as! LoadTableViewCell
            return cell
        }else{
            
            let commentCell = tableView.dequeueReusableCellWithIdentifier("commentCell") as! CommentTableViewCell
            let user : PFUser = object["author"] as! PFUser
            let profilePicture:PFFile = user["profileImage"] as! PFFile
            let username = user["username"] as! String
            var tapEvent = UITapGestureRecognizer(target: self, action: "tappedEvent:")
            
            
            commentCell.commentTextLabel.text = object["text"] as? String
            commentCell.profileImageView.file = profilePicture
            commentCell.usernameLabel.text = username
            commentCell.profileImageView.layer.cornerRadius = 15
            commentCell.profileImageView.layer.masksToBounds = true
            commentCell.profileImageView.loadInBackground(nil)
            return commentCell
        }
    }

    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    
 
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == self.objects!.count){
            return 50.0
        }
        return 80
    }
    override func _indexPathForPaginationCell() -> NSIndexPath {
        return NSIndexPath(forRow: 0, inSection: self.objects!.count)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var sections:NSInteger = self.objects!.count
        if(self.paginationEnabled && sections > 0){
            sections++
        }
        return sections
        
    }
    
    override func tableView(tableView: UITableView, cellForNextPageAtIndexPath indexPath: (NSIndexPath!)) -> PFTableViewCell? {
        var loadMoreCell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell") as! PFTableViewCell
        return loadMoreCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == self.objects!.count && self.paginationEnabled){
            self.loadNextPage()
        }else{
           
        }
    }
    

    
    override func queryForTable() -> PFQuery {
        let query: PFQuery = PFQuery(className: self.parseClassName!)
        // query.includeKey("hostUser")
        query.whereKey("commentId", equalTo: Id)
        query.orderByDescending("createdAt")
        print(query.countObjects())
        //I call a query that takes in hostUser. Where is equal to the hostId.
        
        return query
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
   
        
  
    /*
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
    // Get the new view controller using segue.destinationViewController.
    // Pass the selected object to the new view controller.
    }
    */
    
}
