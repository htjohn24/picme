//
//  CommentTableViewCell.swift
//  Picme
//
//  Created by John Nguyen on 6/19/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class CommentTableViewCell: PFTableViewCell {

    
    @IBOutlet weak var like: UIButton!
    @IBOutlet weak var profileImageView: PFImageView!
    @IBOutlet weak var usernameLabel: UILabel!
    @IBOutlet weak var commentTextLabel: UILabel!
    @IBOutlet weak var numberOfLikes: UILabel!
    
    @IBAction func clickLike(sender: AnyObject) {
        like.selected = !like.selected.boolValue
        let number:Int? = numberOfLikes.text?.toInt() as Int?
        
        if(like.selected == true ){
            numberOfLikes.text = String(number! + 1)
        }else{
            numberOfLikes.text  = String(number! - 1 )
        }
    }
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

}
