//
//  ShareButton.m
//  Picme
//
//  Created by John Nguyen on 8/26/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

#import "ShareButton.h"

@implementation ShareButton

- (id)initWithCoder:(NSCoder *)aDecoder {
    if (self == [super initWithCoder:aDecoder]) {
        [self addTarget:self action:@selector(buttonPressed) forControlEvents:UIControlEventTouchUpInside];
    }
    return self;
}

/*
 // Only override drawRect: if you perform custom drawing.
 // An empty implementation adversely affects performance during animation.
 - (void)drawRect:(CGRect)rect
 {
 // Drawing code
 }
 */

- (void) buttonPressed {
    [self.delegate share:self didTapWithSectionIndex:self.sectionIndex];
}
@end
