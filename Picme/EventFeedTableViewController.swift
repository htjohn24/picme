//
//  FeedTableViewController.swift
//  Picme
//
//  Created by John Nguyen on 4/5/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit
import Parse
import ParseUI
import Cosmos


class EventFeedTableViewController:PFQueryTableViewController, UIGestureRecognizerDelegate  , ProfileButtonDelegate , AttendButtonDelegate , CLLocationManagerDelegate{
    
    var delegate:FilterViewDelegate!
    @IBOutlet weak var menuButton: UIBarButtonItem!
    var eventId = "Uj3eSI72DO"
    
    var index = 10
    var likeArray = NSMutableArray()
    var attendArray = NSMutableArray()
    var commentCount = 0
    var hostId = ""
    var filterKey = ""
    var currentLocation = CLLocation(latitude: 47.6097, longitude: -122.3331)
    let locationManager =  CLLocationManager()
    var currentLocationGeo = PFGeoPoint()
    var proximity = 50.0
    var groupSize = 50
    var eduBool = true
    var verifiedBool = false
    var filterArray : NSArray = NSArray(array: [50,50,2])
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.parseClassName = "Events"
        self.pullToRefreshEnabled = true
        self.paginationEnabled = true
        self.objectsPerPage = 20
        
    }
    
    
    override func viewWillAppear(animated: Bool) {
        let currentUser = PFUser.currentUser()
        if(currentUser != nil){
            currentLocationGeo = PFGeoPoint(latitude: currentLocation.coordinate.latitude, longitude: currentLocation.coordinate.longitude)
        }else{
            self.performSegueWithIdentifier("feedToLogin", sender: self)
        }
        self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        loadObjects()
        let time : NSTimer = NSTimer(timeInterval: 2, target: self , selector: Selector("DataReload"), userInfo: nil, repeats: false)
    }
    
    func DataReload(){
        self.tableView.reloadData()
    }
    
    
    func sendValue(value: String){
        print("data: " + value )
    }
    
    override func viewDidLoad() {
        println("Prox on Feed: " + String(stringInterpolationSegment: proximity))
        locationManager.delegate = self
        locationManager.requestWhenInUseAuthorization()
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadFood:",name:"loadFood", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadSports:",name:"loadSports", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadTrail:",name:"loadTrail", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadGames:",name:"loadGames", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadMusic:",name:"loadMusic", object: nil)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadEdu:",name:"loadEdu", object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "loadClear:",name:"loadClear", object: nil)
         NSNotificationCenter.defaultCenter().addObserver(self, selector: "filterApply:",name:"filterApply", object: nil)
        proximity = filterArray[0] as! Double
        super.viewDidLoad()
    }
    
    
    
    func locationManager(manager:CLLocationManager!, didUpdateLocations locations:[AnyObject]!){
        if let location = locations.first as? CLLocation{
            
            currentLocation = CLLocation(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
            locationManager.stopUpdatingLocation()
        }
    }
    func locationManager(manager:CLLocationManager!, didChangeAuthorizationStatus status: CLAuthorizationStatus){
        if status == .AuthorizedWhenInUse{
            locationManager.startUpdatingLocation()
        }
    }
    
    
    func loadFood(notification: NSNotification){
        filterKey = "food"
        println("Filter Key: " + filterKey)
        
        loadObjects()
    }
    
    func filterApply(notification: NSNotification){
        println("Applying filter")
        proximity = notification.userInfo!["proximity"] as! Double
        groupSize = notification.userInfo!["size"] as! NSInteger
        eduBool = notification.userInfo!["edu"] as! Bool
        verifiedBool = notification.userInfo!["verified"] as!Bool
        println("edu : " + String(stringInterpolationSegment: eduBool))
        println("veri : " + String(stringInterpolationSegment: verifiedBool))
        loadObjects()
    }
    
    func loadSports(notification: NSNotification){
        filterKey = "sports"
        println("Filter Key: " + filterKey)
        
        loadObjects()
    }
    func loadTrail(notification: NSNotification){
        filterKey = "trail"
        println("Filter Key: " + filterKey)
        
        loadObjects()
    }
    func loadGames(notification: NSNotification){
        filterKey = "games"
        println("Filter Key: " + filterKey)
        
        loadObjects()
    }
    
    func loadMusic(notification: NSNotification){
        filterKey = "music"
        println("Filter Key: " + filterKey)
        
        loadObjects()
    }
    
    func loadEdu(notification: NSNotification){
        filterKey = "edu"
        println("Filter Key: " + filterKey)
        
        loadObjects()
    }
    
    func loadClear(notification: NSNotification){
        filterKey = ""
        println("Filter Key: " + filterKey)
        
        loadObjects()
    }
    

    
    override func objectsDidLoad(error: NSError!) {
        super.objectsDidLoad(error)
        if(PFUser.currentUser() != nil){
        let joinQuery : PFQuery = PFQuery(className: "Joins")
        joinQuery.whereKey("userID", equalTo: PFUser.currentUser()!)
        joinQuery.includeKey("eventID")
        joinQuery.findObjectsInBackgroundWithBlock { (activities, error) ->Void in
            if((error) == nil){
                if(activities?.count > 0){
                    for activity in activities!{
                        let event:PFObject = activity["eventID"] as! PFObject
                        self.attendArray.addObject(event.objectId!)
                        self.tableView.reloadData()
                    }
                }
            }else{
                println(error)
            }
        }
        }
    }
    
    override func objectAtIndexPath(indexPath: NSIndexPath!) -> PFObject? {
            return self.objects![indexPath.row] as? PFObject
    }

    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        var sections:NSInteger = self.objects!.count
        return sections
    }
    
    override func tableView(tableView: (UITableView!), cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject!) -> PFTableViewCell? {
       
        let eventCell = tableView.dequeueReusableCellWithIdentifier("Cell") as! EventTableViewCell
        
        let event : PFObject = self.objects![indexPath.row] as! PFObject
        let user : PFUser = event["hostUser"] as! PFUser
        let title : String = event["eventTitle"] as! String
        let profilePicture:PFFile = user["profileImage"] as! PFFile
        let vote : NSInteger = event["Likes"] as! NSInteger
         let xyCoord : PFGeoPoint = event["xyCoord"] as! PFGeoPoint
        let description : NSString = event["eventDescription"] as! NSString
        let eventBefore : PFObject = self.objects![indexPath.row] as! PFObject
        let eventLocation = CLLocation(latitude: xyCoord.latitude, longitude: xyCoord.longitude)
        let groupSize = event["size"] as! NSInteger
        let joins = event["Joins"] as! NSInteger
        let date = event["eventStartDate"] as! NSDate
        
        let dateFormatter = NSDateFormatter()
        dateFormatter.timeStyle = .ShortStyle
        
        let distanceBetweenDates = date.timeIntervalSinceNow
        
        let indexOfMatchedObject = self.attendArray.indexOfObject(event.objectId!)
        if(indexOfMatchedObject == NSNotFound){
            eventCell.attendButton.selected = false
        }else{
            eventCell.attendButton.selected = true
        }
            eventId = object.objectId!
            eventCell.profileImageView.file = object.objectForKey("eventPicture") as? PFFile
            eventCell.distanceLabel.text = String(stringInterpolationSegment: Int(eventLocation.distanceFromLocation(currentLocation)/1609.34)) + " miles"
            eventCell.attendButton.sectionIndex = indexPath.row
            eventCell.attendButton.delegate = self
            eventCell.eventTitleLabel.text = title
            eventCell.hostNameLabel.text = user.username
            eventCell.userReview.rating = user["averageReview"] as! Double
            eventCell.profileImageView.layer.cornerRadius = 27
            eventCell.profileImageView.layer.masksToBounds = true
            eventCell.profileImageView.loadInBackground(nil)
            eventCell.joinsLabel.text = String(joins)
            eventCell.groupSizeLabel.text = "/" + String(groupSize)
            eventCell.timeLabel.text = "In " + String(stringInterpolationSegment: Int(distanceBetweenDates)/3600) + " hrs"
            return eventCell
    }
    
    func tappedEvent(sender : UITapGestureRecognizer){
        self.performSegueWithIdentifier("feedToEvent", sender: self)
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == self.objects!.count && self.paginationEnabled){
            self.loadNextPage()
        }else{
            let object :PFObject = (objects![indexPath.row] as! PFObject)
            eventId = object.objectId!
            self.performSegueWithIdentifier("feedToEvent", sender: self)
        }
    }
    
    override func queryForTable() -> PFQuery {
        var query: PFQuery = PFQuery(className: self.parseClassName!)
       
       query.includeKey("hostUser")
        query.whereKey("eventType", matchesRegex: filterKey)
        query.whereKey("xyCoord", nearGeoPoint: currentLocationGeo, withinMiles: proximity)
        query.whereKey("size", lessThanOrEqualTo: groupSize)

        var userQuery : PFQuery = PFQuery(className: "_User")
        userQuery.whereKey("student", equalTo: eduBool)
        userQuery.whereKey("verified", equalTo: verifiedBool)
        
        query.whereKey("hostUser", matchesQuery: userQuery)
       println("List objects : " + String(userQuery.countObjects()))
        return query
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "feedToEvent"){
            let destinationViewController = segue.destinationViewController as! EventViewController
            destinationViewController.eventID = eventId
            let object : PFObject = PFObject(withoutDataWithClassName: "Events", objectId: eventId)
            let hostId2 = object["hostUser"] as! PFUser
            
            destinationViewController.eventHost =  hostId        }
        println("Host Id in Segue: " + hostId)
        if(segue.identifier == "feedToProfile"){
            let destinationViewController = segue.destinationViewController as! ProfileViewController
            let object : PFObject = PFObject(withoutDataWithClassName: "Events", objectId: eventId)
            let hostId2 = object["hostUser"] as! PFUser
            destinationViewController.profileId = hostId
        }
        
        if(segue.identifier == "eventToSender"){
            let nav = segue.destinationViewController as! UINavigationController
            let destinationViewController = nav.topViewController as! CommentsViewController
            destinationViewController.Id = eventId
            
        }
        
        if(segue.identifier == "eventFeedToConfirmation"){
            let nav = segue.destinationViewController as! UINavigationController
            let destinationViewController = nav.topViewController as! EventConfirmationViewController
            destinationViewController.eventId = eventId
            
        }
        
     
    }
    
    func attend(button: AttendButton!, didTapWithSectionIndex index: Int) {
        let event: PFObject = self.objects![index] as! PFObject
        let host : PFUser = event["hostUser"] as! PFUser
        eventId = event.objectId!
        if(button.selected == false){
            button.selected = true
        self.performSegueWithIdentifier("eventFeedToConfirmation", sender: self)
            
        }else{
           
            unattendEvent(event, user: PFUser.currentUser()!)
            button.selected = false
            
          
        }
        
    }
    
    func unattendEvent(event : PFObject , user: PFUser){
        let query :PFQuery = PFQuery(className: "Joins")
        query.whereKey("userID", equalTo: user)
        query.whereKey("eventID", equalTo: event)
        query.findObjectsInBackgroundWithBlock { (joins, error) -> Void in
            if(error == nil){
                for join in joins!{
                    join.delete()
                    self.attendArray.removeObject(event.objectId!)
                    var joins : Int = event["Joins"] as! Int
                    event.setObject(joins - 1 , forKey: "Joins")
                    event.save()
                }
            }
        }
    }

    
    func profile(button: ProfileButton!, didTapWithSectionIndex index: Int) {
        let event: PFObject = self.objects![index] as! PFObject
        let host : PFUser = event["hostUser"] as! PFUser
        hostId = host.objectId!
        self.performSegueWithIdentifier("feedToProfile", sender: self)
        println("Clicked on profile button")
        
    }
    
   

    
    func joinedEvent(event:PFObject , user: PFUser){
        
    }
    
}


