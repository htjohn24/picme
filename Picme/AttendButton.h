//
//  AttendButton.h
//  Picme
//
//  Created by John Nguyen on 7/5/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AttendButton;
@protocol AttendButtonDelegate
- (void) attend:(AttendButton *)button didTapWithSectionIndex:(NSInteger)index;
@end


@interface AttendButton : UIButton

@property (nonatomic, assign) NSInteger sectionIndex;
@property (nonatomic, weak) id <AttendButtonDelegate> delegate;
@end
