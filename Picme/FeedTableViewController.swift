//
//  FeedTableViewController.swift
//  Picme
//
//  Created by John Nguyen on 4/5/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit
import Parse
import ParseUI

class FeedTableViewController:PFQueryTableViewController, UIGestureRecognizerDelegate, LikeButtonDelegate , AttendButtonDelegate, ShareButtonDelegate , ProfileButtonDelegate {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    var eventId = "Uj3eSI72DO"
    
    var index = 10
    var likeArray = NSMutableArray()
    var attendArray = NSMutableArray()
    var commentCount = 0
    var hostId = ""
  
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        self.parseClassName = "Events"
        self.pullToRefreshEnabled = true
        self.paginationEnabled = true
        self.objectsPerPage = 20
        
    }
    
    override func viewWillAppear(animated: Bool) {
        let currentUser = PFUser.currentUser()
        if(currentUser != nil){

        }else{
            self.performSegueWithIdentifier("feedToLogin", sender: self)
        }
      
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        loadObjects()
        //Make a timer here
        let time : NSTimer = NSTimer(timeInterval: 2, target: self , selector: Selector("DataReload"), userInfo: nil, repeats: false)
    }
    
    func DataReload(){
        self.tableView.reloadData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

    }
    
    override func objectsDidLoad(error: NSError!) {
        super.objectsDidLoad(error)
        if(PFUser.currentUser() != nil){
            let query : PFQuery = PFQuery(className: "Likes")
            
            
            query.whereKey("userID", equalTo: PFUser.currentUser()!)
            query.includeKey("eventID")
            query.findObjectsInBackgroundWithBlock ({ (activities, error) ->Void in
                if((error) == nil){
                    if(activities?.count > 0){
                        for activity in activities!{
                            let event:PFObject = activity["eventID"] as! PFObject
                            self.likeArray.addObject(event.objectId!)
                            self.tableView.reloadData()
                        }
                    }
                  
                }else{
                    println(error)
                }
            })
            
            let queryTwo : PFQuery = PFQuery(className: "Joins")
            queryTwo.whereKey("userID", equalTo: PFUser.currentUser()!)
            queryTwo.includeKey("eventID")
            queryTwo.findObjectsInBackgroundWithBlock { (activities, error) ->Void in
                if((error) == nil){
                    if(activities?.count > 0){
                        for activity in activities!{
                            let event:PFObject = activity["eventID"] as! PFObject
                            self.attendArray.addObject(event.objectId!)
                            self.tableView.reloadData()
                        }
                    }
                    
                }else{
                    println(error)
                }
            }
        }
    }
    
    override func objectAtIndexPath(indexPath: NSIndexPath!) -> PFObject? {
        if(indexPath.section < self.objects!.count){
            return self.objects![indexPath.section] as? PFObject
        }else{
            return nil
        }
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if(section == self.objects!.count){
            return nil
        }
        
        let sectionHeaderView : HeaderTableViewCell = tableView.dequeueReusableCellWithIdentifier("SectionHeaderCell") as! HeaderTableViewCell
        
        let profileImageView : PFImageView = sectionHeaderView.viewWithTag(1) as! PFImageView
        let usernameLabel : UILabel = sectionHeaderView.viewWithTag(2) as! UILabel
        let descriptionLabel : KILabel = sectionHeaderView.viewWithTag(3) as! KILabel
        let voteLabel : UILabel = sectionHeaderView.viewWithTag(4) as! UILabel
    
        sectionHeaderView.like.sectionIndex = section
        var tapProfile = UITapGestureRecognizer(target: self, action: "tappedProfile:")
        tapProfile.numberOfTapsRequired = 1
        profileImageView.userInteractionEnabled = true
        profileImageView.tag = section
        profileImageView.addGestureRecognizer(tapProfile)
        
        descriptionLabel.systemURLStyle = true
        descriptionLabel.hashtagLinkTapHandler = { label, hashtag, range in
            let alert : UIAlertController = UIAlertController(title: "Tapped" + hashtag, message: "Tapped" + hashtag, preferredStyle: UIAlertControllerStyle.Alert)
            NSLog("Hashtah \(hashtag) tapped")
        }
       
        
        let event : PFObject = self.objects![section] as! PFObject
        let user : PFUser = event["hostUser"] as! PFUser
        let title : NSString = event["eventTitle"] as! NSString
        let profilePicture:PFFile = user["profileImage"] as! PFFile
        let vote : NSInteger = event["Likes"] as! NSInteger
        let description : NSString = event["eventDescription"] as! NSString
        let eventBefore : PFObject = self.objects![section] as! PFObject
        let currentUser : PFUser = eventBefore["hostUser"] as! PFUser
        hostId = currentUser.objectId!
        sectionHeaderView.profile.delegate = self
        sectionHeaderView.profile.sectionIndex = section
        println("Host Id in header section: " + hostId)
        let indexOfMatchedObject = self.likeArray.indexOfObject(event.objectId!)
        if(indexOfMatchedObject == NSNotFound){
            sectionHeaderView.like.selected = false
        }else{
            sectionHeaderView.like.selected = true
        }
        sectionHeaderView.like.delegate = self
        sectionHeaderView.like.sectionIndex = section
        
        
        
        usernameLabel.text = title as String
        descriptionLabel.text = description as String
        profileImageView.file = profilePicture
        voteLabel.text = String(vote)
        profileImageView.layer.cornerRadius = 20
        profileImageView.layer.masksToBounds = true
        profileImageView.loadInBackground(nil)
        
        return sectionHeaderView
    }
    
    func tappedProfile(sender : UITapGestureRecognizer){
        //self.performSegueWithIdentifier("feedToProfile", sender: self)
    }
    
   
    
    func gestureRecognizerShouldBegin(gestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    override func tableView(tableView: (UITableView!), cellForRowAtIndexPath indexPath: NSIndexPath, object: PFObject!) -> PFTableViewCell? {
        if(indexPath.section == self.objects!.count)
        {
            var cell:LoadTableViewCell = self.tableView(tableView, cellForNextPageAtIndexPath: indexPath)! as! LoadTableViewCell
            return cell
            
        }else
        {
            let imageCell = tableView.dequeueReusableCellWithIdentifier("PhotoCell") as! BodyTableViewCell
            eventId = object.objectId!
            imageCell.eventImageView.file = object.objectForKey("eventPicture") as? PFFile
            imageCell.eventImageView.loadInBackground({ (image, error) -> Void in
               
            })
            return imageCell
        }
    }
    
    func tappedEvent(sender : UITapGestureRecognizer){
        self.performSegueWithIdentifier("feedToEvent", sender: self)
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        var sections:NSInteger = self.objects!.count
        if(self.paginationEnabled && sections > 0){
            sections++
        }
        return sections
    }
    
    override func _indexPathForPaginationCell() -> NSIndexPath {
        return NSIndexPath(forRow: 0, inSection: self.objects!.count)
    }
    
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        if(section == self.objects!.count){
            return 0
        }
        return 50.0
    }
    
    override func tableView(tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        if(section == self.objects!.count){
            return 0
        }
        return 50.0
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        if(indexPath.section == self.objects!.count){
            return 50.0
        }
        return 320.0
    }
    
    override func tableView(tableView: UITableView, cellForNextPageAtIndexPath indexPath: (NSIndexPath!)) -> PFTableViewCell? {
        var loadMoreCell = tableView.dequeueReusableCellWithIdentifier("LoadMoreCell") as! PFTableViewCell
        return loadMoreCell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == self.objects!.count && self.paginationEnabled){
            self.loadNextPage()
        }else{
            let object :PFObject = (objects![indexPath.section] as! PFObject)
            eventId = object.objectId!
            self.performSegueWithIdentifier("feedToEvent", sender: self)
        }
    }
    
    override func tableView(tableView: UITableView, viewForFooterInSection section: Int) -> UIView? {
        let footerView = tableView.dequeueReusableCellWithIdentifier("FooterView") as! FooterTableViewCell
        footerView.attend.delegate = self
        footerView.attend.sectionIndex = section
        footerView.share.delegate = self
        footerView.share.sectionIndex = section
        let event : PFObject = self.objects![section] as! PFObject
        //footerView.commentLabel.text = "Test"
        let indexOfMatchedObject = self.attendArray.indexOfObject(event.objectId!)
        if(indexOfMatchedObject == NSNotFound){
            footerView.attend.selected = false
        }else{
            footerView.attend.selected = true
        }
       // let comments :PFQuery = PFQuery(className: "Comments")
      //  comments.whereKey("commentId", equalTo: eventId)
  
       //footerView.commentsLabel.text = String(comments.countObjects())
        return footerView
    }
    
    @IBAction func addEvent(sender: AnyObject) {
        self.performSegueWithIdentifier("feedToCamera", sender: self)
    }
    
    override func queryForTable() -> PFQuery {
        let query: PFQuery = PFQuery(className: self.parseClassName!)
        query.includeKey("hostUser")
        query.orderByDescending("createdAt")
        
        return query
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "feedToEvent"){
            let destinationViewController = segue.destinationViewController as! EventViewController
            destinationViewController.eventID = eventId
            let object : PFObject = PFObject(withoutDataWithClassName: "Events", objectId: eventId)
            let hostId2 = object["hostUser"] as! PFUser
           
            destinationViewController.eventHost =  hostId        }
            println("Host Id in Segue: " + hostId)
        if(segue.identifier == "feedToProfile"){
            let destinationViewController = segue.destinationViewController as! ProfileViewController
            let object : PFObject = PFObject(withoutDataWithClassName: "Events", objectId: eventId)
            let hostId2 = object["hostUser"] as! PFUser
            destinationViewController.profileId = hostId
        }
        
        if(segue.identifier == "eventToSender"){
            let nav = segue.destinationViewController as! UINavigationController
            let destinationViewController = nav.topViewController as! CommentsViewController
            destinationViewController.Id = eventId
            
        }
    }
    
    
    func like(button: LikeButton!, didTapWithSectionIndex index: Int) {
        let event : PFObject = self.objects![index] as! PFObject
        let user : PFUser = PFUser.currentUser()!
        //Check if button is selected or not
        if(button.selected == true){
          self.unlikeEvent(event , user: user)
       
        }else{
          self.likeEvent(event , user: user)
        }
    }
    
    func attend(button: AttendButton!, didTapWithSectionIndex index: Int) {
        let event: PFObject = self.objects![index] as! PFObject
        let user : PFUser = PFUser.currentUser()!
          button.selected = !button.selected.boolValue
        if(button.selected == true){
            self.unattendEvent(event, user: user)
        }else{
            self.attendEvent(event, user: user)
        }
    }
    
    func profile(button: ProfileButton!, didTapWithSectionIndex index: Int) {
        let event: PFObject = self.objects![index] as! PFObject
        let host : PFUser = event["hostUser"] as! PFUser
        hostId = host.objectId!
        self.performSegueWithIdentifier("feedToProfile", sender: self)
        println("Clicked on profile button")
        
    }
    
    func share(button: ShareButton!, didTapWithSectionIndex index: Int) {
        let event : PFObject = self.objects![index] as! PFObject
        let eventTitle : String = event["eventTitle"] as! String
        var alert:UIAlertController=UIAlertController(title: "Share Event", message: nil, preferredStyle: UIAlertControllerStyle.ActionSheet)
        
        var facebookEvent = UIAlertAction(title: "Facebook", style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.facebook(eventTitle)
                
        }
        var tweetEvent = UIAlertAction(title: "Tweet", style: UIAlertActionStyle.Default)
            {
                UIAlertAction in
                self.tweet(eventTitle)
        }
        var cancelAction = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.Cancel)
            {
                UIAlertAction in
                
        }
        // Add the actions
        alert.addAction(facebookEvent)
        alert.addAction(tweetEvent)
        
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone
        {
            self.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    
    

    
    
    func facebook(title:String){
        
        var SocialMedia :SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
        SocialMedia.setInitialText("I'm going to an event named " + title + ". Check out my moments using the PicMe App")
        self.presentViewController(SocialMedia, animated: true, completion: nil)
        
    }
    
    func tweet(title: String){
        
        var SocialMedia :SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeTwitter)
        SocialMedia.setInitialText("I'm going to an event named " + title  + "Check out my moments using the PicMe App")
        self.presentViewController(SocialMedia, animated: true, completion: nil)
      
        
    }
    
    
    func attendEvent(event: PFObject , user: PFUser){
        let activity : PFObject = PFObject(className: "Joins")
        activity["eventID"] = event
        activity["userID"] = user
        activity.save()
        var joins : Int = event["Joins"] as! Int
        event.setObject(joins + 1 , forKey: "Joins")
        event.save()
    }
    
    func unattendEvent(event : PFObject , user: PFUser){
        let query :PFQuery = PFQuery(className: "Joins")
        query.whereKey("userID", equalTo: user)
        query.whereKey("eventID", equalTo: event)
        query.findObjectsInBackgroundWithBlock { (joins, error) -> Void in
            if(error == nil){
                for join in joins!{
                    join.delete()
                    self.attendArray.removeObject(event.objectId!)
                    var joins : Int = event["Joins"] as! Int
                    event.setObject(joins - 1 , forKey: "Joins")
                    event.save()
                }
            }
        }
    }
    
    func likeEvent(event:PFObject , user: PFUser){
        let activity : PFObject = PFObject(className: "Likes")
        activity["eventID"] = event
        activity["userID"] = user
        activity.save()
        
        var likes : Int = event["Likes"] as! Int
        event.setObject(likes + 1 , forKey: "Likes")
        event.save()
        
    }


    func unlikeEvent(event:PFObject , user: PFUser){
        let query :PFQuery = PFQuery(className: "Likes")
        query.whereKey("userID", equalTo: user)
        query.whereKey("eventID", equalTo: event)
        query.findObjectsInBackgroundWithBlock { (likes, error) -> Void in
            if(error == nil){
                for like in likes!{
                    like.delete()
                    self.likeArray.removeObject(event.objectId!)
                    var likes : Int = event["Likes"] as! Int
                    event.setObject(likes - 1 , forKey: "Likes")
                    event.save()
                }
            }
        }
    }
    
    func update(events: PFObject) {
        let query: PFQuery = PFQuery(className: "Events")
        query.whereKey("user", equalTo: PFUser.currentUser()!)
        let event : PFObject = events as PFObject
        let likes = event["Likes"] as! NSInteger
        query.getObjectInBackgroundWithId(event.objectId!, block: { (events, error) -> Void in
            if(error == nil){
              
            }else{
            
            }
        })
    }
    
    

  
    func joinedEvent(event:PFObject , user: PFUser){
    
    }
    
}
