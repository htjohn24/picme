//
//  EventViewController.swift
//  Picme
//
//  Created by John Nguyen on 4/11/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit

class EventViewController: UIViewController  {

    @IBOutlet weak var menuButton: UIBarButtonItem!
    @IBOutlet weak var eventTitleLabel: UILabel!
    @IBOutlet weak var eventDescriptionLabel: UILabel!
    @IBOutlet weak var eventLikesLabel: UILabel!
    @IBOutlet weak var eventJoinsLabel: UILabel!
    @IBOutlet weak var trashButton: UIButton!
    @IBOutlet weak var mapView: GMSMapView!
    
    var eventMoments = []
    var eventTitle = "Churrios"
    var eventDescription = "Cherries are happy fruits"
    var eventAttendees = 0
    var eventLove = 0
    var eventShares = 0
    var eventID = "OB1WgiTrBS"
    var eventJoins = 0
    var eventHost = "Rich Del Ray"
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let object : PFObject = PFObject(withoutDataWithClassName: "Events", objectId: eventID)
        eventTitle = object["eventTitle"] as! String
        eventDescription = object["eventDescription"] as! String
        eventLove = object["Likes"] as! Int
        eventJoins = object["Joins"] as! Int
        let xyCoord : PFGeoPoint = object["xyCoord"] as! PFGeoPoint
        let position : CLLocationCoordinate2D = CLLocationCoordinate2DMake(xyCoord.latitude, xyCoord.longitude)
        eventTitleLabel.text = eventTitle
        eventDescriptionLabel.text = eventDescription
        eventLikesLabel.text = String(eventLove)
        eventJoinsLabel.text = String(eventJoins)
        mapView.camera = GMSCameraPosition(target: position, zoom: 15, bearing: 0, viewingAngle: 0.0)
        var marker = GMSMarker()
        marker.position = position
        marker.map = mapView
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = "revealToggle:"
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
    
        if(eventHost != PFUser.currentUser()?.objectId){
            trashButton.hidden = true
        }
    }
   
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    //Pass data to object within another view. 
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "eventToMoments"){
            let destinationViewController = segue.destinationViewController as! MomentsTableViewController
            destinationViewController.eventId = eventID
        }
        
        if(segue.identifier == "eventToCapture"){
            let destinationViewController = segue.destinationViewController as! CaptureViewController
            destinationViewController.eventId = eventID
        }
        
    }
    
    //Description: Query and Delete like ,  Join , and then the event itself which contains the likes and
    @IBAction func deleteEvent(sender: AnyObject) {
        let likeQuery :PFQuery = PFQuery(className: "Likes")
        likeQuery.whereKey("objectId", equalTo: eventID)
        likeQuery.findObjectsInBackgroundWithBlock { (likes, error) -> Void in
            if(error == nil){
                for like in likes!{
                    like.delete()
                }
            }
        }
        
        let joinQuery :PFQuery = PFQuery(className: "Joins")
        joinQuery.whereKey("ObjectId", equalTo: eventID)
        joinQuery.findObjectsInBackgroundWithBlock { (joins, error) -> Void in
            if(error == nil){
                for join in joins!{
                    join.delete()
                }
            }
        }

        let query :PFQuery = PFQuery(className: "Events")
        query.whereKey("hostUser", equalTo: PFUser.currentUser()!)
        query.whereKey("objectId", equalTo: eventID)
        query.findObjectsInBackgroundWithBlock { (events, error) -> Void in
            if(error == nil){
                for event in events!{
                    event.delete()
                    self.navigationController?.popViewControllerAnimated(true)
                }
            }else{
            }
        }
        
     
    }

}
