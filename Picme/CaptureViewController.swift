//
//  CaptureViewController.swift
//  Picme
//
//  Created by John Nguyen on 6/17/15.
//  Copyright (c) 2015 John Nguyen. All rights reserved.
//

import UIKit
import MobileCoreServices
import Parse
import MapKit

class CaptureViewController: UIViewController , UIImagePickerControllerDelegate , UINavigationControllerDelegate {

    var eventId = ""
    let imagePicker = UIImagePickerController()
    
    @IBOutlet weak var captureMomentView: UIImageView!
    @IBOutlet weak var captionTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(animated: Bool) {
        self.imagePicker.delegate = self
        self.imagePicker.allowsEditing = true
        
        if (UIImagePickerController.isSourceTypeAvailable(UIImagePickerControllerSourceType.Camera)){
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.Camera
        }else{
            self.imagePicker.sourceType = UIImagePickerControllerSourceType.PhotoLibrary
        }
        self.imagePicker.mediaTypes = NSArray(objects: kUTTypeImage) as [AnyObject]
        self.presentViewController(self.imagePicker, animated: false, completion: nil)

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [NSObject : AnyObject]) {
        var chosenImage:UIImage = info[UIImagePickerControllerOriginalImage] as! UIImage
            self.captureMomentView.image = chosenImage
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    @IBAction func share(sender: AnyObject) {
        if((captureMomentView.image) != nil){
            
        let imageData : NSData = UIImagePNGRepresentation(scaleImage(captureMomentView.image!, newSize: CGSizeMake(222, 222) )) as NSData
        let photoFile : PFFile = PFFile(data: imageData)
        var eventPictures : PFObject = PFObject(className: "EventPictures")
            
            eventPictures["FileName"] = photoFile
            eventPictures["eventID"] = eventId
            eventPictures["hostUser"] = PFUser.currentUser()
            eventPictures["caption"] = self.captionTextField.text as String
            eventPictures["votes"] = 0
            eventPictures.saveInBackgroundWithBlock({ (success, error) -> Void in
                println("Success")
                self.performSegueWithIdentifier("captureToEvent", sender: self)
            })
        }
        
    }
    
    func scaleImage(image: UIImage, newSize: CGSize) -> UIImage {
        
        var scaledSize = newSize
        var scaleFactor: CGFloat = 1.0
        
        if image.size.width > image.size.height {
            scaleFactor = image.size.width / image.size.height
            scaledSize.width = newSize.width
            scaledSize.height = newSize.width / scaleFactor
        } else {
            scaleFactor = image.size.height / image.size.width
            scaledSize.height = newSize.height
            scaledSize.width = newSize.width / scaleFactor
        }
        
        UIGraphicsBeginImageContextWithOptions(scaledSize, false, 0.0)
        let scaledImageRect = CGRectMake(0.0, 0.0, scaledSize.width, scaledSize.height)
        [image .drawInRect(scaledImageRect)]
        let scaledImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return scaledImage
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if(segue.identifier == "captureToEvent"){
            let destinationViewController = segue.destinationViewController as! EventViewController
            
            destinationViewController.eventID = eventId
        }
        
    }
    

}
